package org.anyline.simple.oracle;

import org.anyline.metadata.Catalog;
import org.anyline.metadata.Schema;
import org.anyline.metadata.Table;
import org.anyline.service.AnylineService;
import org.anyline.util.ConfigTable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootTest
public class PartitionTest {
    private Logger log = LoggerFactory.getLogger(PartitionTest.class);
    @Autowired
    private AnylineService service          ;
    @Autowired
    private JdbcTemplate jdbc               ;
    private Catalog catalog = null          ; //
    private Schema schema   = null          ; //相当于数据库名  查数据库列表 是用SHOW SCHEMAS 但JDBC con.getCatalog()返回数据库名 而con.getSchema()返回null
    private String table    = "crm_user_master"    ; // 表名



    /**
     * 分区
     * PARTITION BY RANGE(ID)(
     * 	PARTITION s1 VALUES LESS THAN (100)
     * 	, PARTITION s2 VALUES LESS THAN (200)
     * 	, PARTITION s3 VALUES LESS THAN (300)
     * )
     * @throws Exception 异常
     */
    @Test
    public void partition_less() throws Exception{
        ConfigTable.IS_LOG_ADAPTER_MATCH = true;
        String name = "partition_less";
        Table table = head(name);
        Table.Partition partition = new Table.Partition();
        partition.addColumn("ID");//.addColumn("QTY");
        partition.setType(Table.Partition.TYPE.RANGE);
        partition.addSlice(new Table.Partition.Slice("s1").setLess("ID",100));//.setLess("QTY",100));
        partition.addSlice(new Table.Partition.Slice("s2").setLess("ID",200));//.setLess("QTY",200));
        partition.addSlice(new Table.Partition.Slice("s3").setLess("ID",300));//.setLess("QTY",1000));
        table.setPartition(partition);
        service.ddl().create(table);
        //table = service.metadata().table(name);
        //partition = table.getPartition();
        //Table.Partition.TYPE type = partition.getType();
    }


    /**
     * 分区
     * PARTITION BY LIST(ID)
     * ( PARTITION s1 VALUES IN(1,2)
     * , PARTITION s2 VALUES IN(11,12)
     * )
     * @throws Exception 异常
     */
    @Test
    public void partition_list() throws Exception{
        String name = "partition_list1";
        Table table = head(name);
        Table.Partition partition = new Table.Partition();
        partition.addColumn("ID");
        partition.setType(Table.Partition.TYPE.LIST);
        partition.addSlice(new Table.Partition.Slice(name+"_s11").addValues(0).addValues(99));
        partition.addSlice(new Table.Partition.Slice(name+"_s12").addValues(100).addValues(999));
        partition.addSlice(new Table.Partition.Slice(name+"_s13").addValues(1000).addValues(9999));
        table.setPartition(partition);
        service.ddl().create(table);
        //table = service.metadata().table(name);
        //partition = table.getPartition();
        //Table.Partition.TYPE type = partition.getType();
    }
    /**
     * PARTITION BY HASH(ID) PARTITIONS 100
     * @throws Exception 异常
     */
    @Test
    public void partition_hash() throws Exception{
        String name = "partition_hash1";
        Table table = head(name);
        Table.Partition partition = new Table.Partition();
        partition.addColumn("ID");
        partition.setType(Table.Partition.TYPE.HASH).setModulus(100);
        table.setPartition(partition);
        service.ddl().create(table);
        //table = service.metadata().table(name);
        //partition = table.getPartition();
        //Table.Partition.TYPE type = partition.getType();
    }

    public Table head(String name) throws Exception{

        ConfigTable.IS_THROW_SQL_UPDATE_EXCEPTION = true; //遇到SQL异常直接抛出
        //检测表结构
        Table table = service.metadata().table(catalog, schema, name, false);
        //如果存在则删除
        if(null != table){
            service.ddl().drop(table);
        }

        //再查询一次
        table = service.metadata().table(catalog, schema, name);
        Assertions.assertNull(table);

        //定义表结构
        table = new Table(name);
        table.setComment("表备注");
        //设置分桶方式 DISTRIBUTED BY HASH('ID'） BUCKETS 2
        table.setDistribution(Table.Distribution.TYPE.HASH, 3, "ID");

        table.addColumn("ID", "INT", false, null).setComment("主键");//.autoIncrement(true).primary(true);
        table.addColumn("QTY", "INT").setComment("数量");
        table.addColumn("CODE", "VARCHAR(10)").setComment("编号");
        table.addColumn("CODE2", "double(10)").setComment("编号");
        table.addColumn("NAME", "VARCHAR(20)").setComment("名称");
        table.addColumn("REG_TIME", "date").setComment("注册时间");
        table.addColumn("REG_TIME1", "datetime").setComment("注册时间");
        table.addColumn("REG_TIME2", "timestamp(6)").setComment("注册时间");
        table.addColumn("DATA_VERSION", "double").setComment("数据版本");
        return table;
    }
}
