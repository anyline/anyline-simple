package org.anyline.simple.ignite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class IgniteApplication {
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(IgniteApplication.class);
		ConfigurableApplicationContext context = application.run(args);
	}
}
