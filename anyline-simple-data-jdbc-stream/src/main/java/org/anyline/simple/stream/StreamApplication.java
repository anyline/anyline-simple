package org.anyline.simple.stream;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamApplication {

	public static void main(String[] args) throws Exception{
		SpringApplication application = new SpringApplication(StreamApplication.class);
		application.run(args);

	}

}
