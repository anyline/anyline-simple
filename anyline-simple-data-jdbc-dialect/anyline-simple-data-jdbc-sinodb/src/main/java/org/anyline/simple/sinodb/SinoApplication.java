package org.anyline.simple.sinodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SinoApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SinoApplication.class);
        application.run(args);
    }
}
