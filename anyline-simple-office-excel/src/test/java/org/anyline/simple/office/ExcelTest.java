package org.anyline.simple.office;

import org.anyline.office.xlsx.entity.XRow;
import org.anyline.office.xlsx.entity.XSheet;
import org.anyline.office.xlsx.entity.XWorkBook;
import org.anyline.util.DomUtil;
import org.anyline.util.FileUtil;
import org.dom4j.DocumentException;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

public class ExcelTest {
    public static void main(String[] args) throws Exception {
       // format();
        insert();
    }
    //源文件格式化
    public static void format() throws DocumentException {
        File dir = new File("E:\\template\\excel");
        List<File> files = FileUtil.getAllChildrenFile(dir, ".rels", ".xml");
        for(File file:files) {
            String content = FileUtil.read(file, "UTF-8").toString();
            String xml = DomUtil.format(content);
            System.out.println(file.getAbsolutePath().replace(dir.getAbsolutePath(), "").replace("\\","/"));
            if(null != xml){
                xml = xml.replaceAll("\\n\\s*\\n", "\n");
                FileUtil.write(xml, file);
            }
           // System.out.println(xml);
        }
    }
    public void test(){

        File template = new File("E:\\template\\excel\\b.xlsx");
        File file = new File("E:\\template\\excel\\b_"+System.currentTimeMillis()+".xlsx");
        FileUtil.copy(template, file);
        XWorkBook book = new XWorkBook(file);
        book.replace("ab_.bc", "替换新值");
        book.replace("${a}", "替换新值B6");
        book.replace("${city}", "青岛+日照");
        //添加一行
        XRow row = book.sheet(2).append(Arrays.asList("a", "b", "c"));
        book.save();

    }
    public static void placeholder(){
        File template = new File("E:\\template\\excel\\b.xlsx");
        File file = new File("E:\\template\\excel\\b_"+System.currentTimeMillis()+".xlsx");
        FileUtil.copy(template, file);
        XWorkBook book = new XWorkBook(file);
        LinkedHashMap<String, XSheet> sheets = book.sheets();
        for(XSheet sheet:sheets.values()){
            System.out.println(sheet.placeholders());
        }
    }
    public static void insert(){
        File template = new File("E:\\template\\excel\\b.xlsx");
        File file = new File("E:\\template\\excel\\b_"+System.currentTimeMillis()+".xlsx");
        FileUtil.copy(template, file);
        XWorkBook book = new XWorkBook(file);
        LinkedHashMap<String, XSheet> sheets = book.sheets();
        for(XSheet sheet:sheets.values()){
            List<XRow> rows = sheet.rows();
            for(XRow row:rows){
                System.out.println(row.placeholders());
            }
            List<Object> vs = new ArrayList<>();
            vs.add(1);
            vs.add(1);
            vs.add(1);
            vs.add(1);
            sheet.append(vs);
        }
        book.save();
    }
}
