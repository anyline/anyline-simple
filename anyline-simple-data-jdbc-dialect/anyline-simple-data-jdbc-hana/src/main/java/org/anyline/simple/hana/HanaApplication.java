package org.anyline.simple.hana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HanaApplication  {
    public static void main(String[] args){
        SpringApplication application = new SpringApplication(HanaApplication.class);
        application.run(args);
    }
}
