## 环境配置
```
这个项目中依赖太多了，有可能运行不起来，先install一下anyline-simple-dependency
如果还运行不了 就去运行这个最简单的环境， 这里主要是用参考详细的语法 https://gitee.com/anyline/anyline-simple-clear 
或者到群里说一下
入门可以看 http://doc.anyline.org/ss/f5_1150     
```
## 示例代码


   ```
        入门请先看  anyline-simple-hello(没有web环境 只操作数据库)
        
        SpringApplication application = new SpringApplication(HelloApplication.class);
        ConfigurableApplicationContext ctx = application.run(args);
        AnylineService service = ServiceProxy.service();
        DataSet set = service.querys("表名");
        System.out.println(set.size());


        项目中操作数据库只需要依赖anyline-data-jdbc-*以及相应数据库的官方驱动
 
        <groupId>org.anyline</groupId>

        版本号参考
        https://mvnrepository.com/artifact/org.anyline/anyline-core
        

        为了操作数据库，需要一个AnylineService，大部分操作通过这个service来完成

        可以在Controller中注入service 也可以不注入 直接使用ServiceProxy.service() ServiceProxy.service("数据源名称")
        @Lazy
        @Qualifier("anyline.service") 
        private AnylineService service;
 

        接下来大部分操作通过这个service来完成，如
        DataSet set = service.querys("HR_USER");
        
        各种JDBC类的方言示例 在 <module>anyline-simple-data-jdbc-dialect</module>
   ```


## 目录说明

        <!--基础依赖-->
        <module>anyline-simple-dependency</module>
        <module>anyline-simple-start</module>
        <module>anyline-simple-start-mysql</module>
        <module>anyline-simple-start-mvc-mysql</module>

        <!--一个完全独立的项目，与其他模块不相关，用为演示快速环境搭建-->
        <module>anyline-simple-alpha-clear</module>

        <!--先执行这个初始化数据库中的表及测试数据-->
        <module>anyline-simple-alpha-init</module>

        <!--一个简单的入门示例-->
        <module>anyline-simple-hello</module>

        <!--多数据源库操作-->
        <module>anyline-simple-data-jdbc-ds</module>

        <!--对结果集的操作-->
        <module>anyline-simple-data-jdbc-result</module>

        <!--pdf操作-->
        <module>anyline-simple-pdf</module>

        <!--正则表达式 主要用来抽取标签 拆分字符串 比如从一段html中抽取所有超链接-->
        <module>anyline-simple-regular</module>

        <!--各种场景的查询-->
        <module>anyline-simple-data-jdbc-query</module>

        <!--各种查询条件构造-->
        <module>anyline-simple-data-condition</module>

        <!--网络操作-->
        <module>anyline-simple-net</module>

        <!--word excel操作 重点实现word excel中的表格操作以及html/css转word标签-->
        <module>anyline-simple-office-excel</module>
        <module>anyline-simple-office-word</module>

        <!--基于springboot的rabbitmq-->
        <module>anyline-simple-rabbitmq</module>
        <!--基于官方驱动的rabbitmq-->
        <module>anyline-simple-rabbitmq-init</module>

        <module>anyline-simple-minio</module>
        <!--没有web环境-->
        <module>anyline-simple-noweb</module>

        <!--各种数据库操作示例-->
        <module>anyline-simple-data-jdbc-dialect</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-dm</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-oracle</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-postgresql</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-clickhouse</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-kingbase</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-sqlite</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-derby</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-h2</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-hana</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-hive</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-hsqldb</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-tdengine</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-mssql</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-mysql</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-mariadb</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-db2</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-questdb</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-timescale</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-neo4j</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-oceanbase-mysql</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-oceanbase</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-opengauss</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-gbase8s</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-oscar</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-informix</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-ignite</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-postgis</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-voltdb</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-iotdb</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-influxdb</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-highgo</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-doris</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-sinodb</module>
        <module>anyline-simple-data-jdbc-dialect/anyline-simple-data-jdbc-nebula</module>
        <module>anyline-simple-data-nebula</module>
        <module>anyline-simple-data-influx</module>

        <module>anyline-simple-data-mongodb</module>
        <module>anyline-simple-data-es</module>

        <!--短信-->
        <module>anyline-simple-sms</module>

        <!--针对Entity的操作-->
        <module>anyline-simple-data-jdbc-entity</module>

        <!--ThingsBoard
        <module>anyline-simple-thingsboard</module>
 
        <!--腾讯地图-->
        <module>anyline-simple-qq-map</module>

        <!--模拟浏览器执行js-->
        <module>anyline-simple-spider</module>

        <!--事务回滚-->
        <module>anyline-simple-transaction</module>

        <!--动态数据源事务回滚-->
        <module>anyline-simple-data-jdbc-transaction</module>

        <!--适配 高德、百度、腾讯地图接口 超限额后自动切换平台-->
        <module>anyline-simple-map</module>

        <!--多表操作-->
        <module>anyline-simple-data-jdbc-tables</module>

        <!--界定符-->
        <module>anyline-simple-data-jdbc-delimiter</module>

        <!--界定符 占位-->
        <module>anyline-simple-data-jdbc-placeholder</module>

        <!--数据库结构 表、列明细-->
        <module>anyline-simple-data-jdbc-metadata</module>

        <!--部分不常用的操作-->
        <module>anyline-simple-data-jdbc-special</module>

        <!--DDL相关操作  创建修改表列数据类型 以及超表子表-->
        <module>anyline-simple-data-jdbc-ddl</module>

        <module>anyline-simple-help</module>
        <!--MDL相关-->
        <module>anyline-simple-data-jdbc-dml</module>
        <!--word操作-->
        <module>anyline-simple-doc</module>
        <!--加密解密rsa m2等-->
        <module>anyline-simple-encrypt</module>
        <!--xml中自定义复杂SQL-->
        <module>anyline-simple-data-jdbc-xml</module>
        <!--java中自定义SQL-->
        <module>anyline-simple-data-jdbc-sql</module>
        <module>anyline-simple-data-jdbc-delete</module>
        <module>anyline-simple-kafka</module>
        <module>anyline-simple-rabbitmq-boot</module>
        <module>anyline-simple-redis</module>
        <module>anyline-simple-data-jdbc-interceptor</module>
        <module>anyline-simple-data-jdbc-id</module>
        <module>anyline-simple-data-jdbc-proxy</module>
        <module>anyline-simple-jar</module>
        <module>anyline-simple-data-jdbc-listener</module>
        <module>anyline-simple-data-jdbc-validate</module>
        <module>anyline-simple-web</module>
        <module>anyline-simple-data-all</module>
        <module>anyline-simple-data-generator</module>
        <module>anyline-simple-data-jdbc-stream</module>
        <module>anyline-simple-office</module>
        <module>anyline-simple-data-jdbc-sharding</module>
        <module>anyline-simple-origin</module>
        <module>anyline-simple-solon</module>
        <module>anyline-simple-vertx</module>
        <module>anyline-simple-data-compare</module>
        <module>anyline-simple-glm</module>
        <module>anyline-ext-dashscope</module>
        <module>anyline-simple-invoice</module>


每个目录下有read说明了实现的示例、测试方式和注意事项