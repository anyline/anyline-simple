package org.anyline.simple.sql;

import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.data.prepare.RunPrepare;
import org.anyline.entity.Compare;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.metadata.Column;
import org.anyline.metadata.Table;
import org.anyline.proxy.ServiceProxy;
import org.anyline.service.AnylineService;
import org.anyline.util.ConfigTable;
import org.anyline.util.regular.Regular;
import org.anyline.util.regular.RegularUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
public class SQLTest {

    @Autowired
    @Qualifier("anyline.service")
    private AnylineService service;

    @Test
    public void pks() throws Exception {
        Table table = service.metadata().table("TAB_PKS", false);
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("TAB_PKS");
        table.addColumn("ID1", "INT").setPrimary(true);
        table.addColumn("ID2", "INT").setPrimary(true);
        table.addColumn("CODE", "varchar(10)");
        service.ddl().create(table);

        DataSet set = new DataSet();
        for(int i=0; i<10; i++){
            DataRow row = set.add();
            row.put("ID1", i);
            row.put("ID2", i*2);
            row.put("CODE", "c"+i);
        }
        set.setPrimaryKey("ID1", "ID2");
        service.save(table, set);

    }
    @Test
    public void empty(){
        ConfigStore configs = new DefaultConfigStore();
        configs.and(Compare.EMPTY_VALUE_SWITCH.SRC, "CODE", "null");
        ConfigStore c = new DefaultConfigStore();
        c.and(configs);
        ConfigStore cc = new DefaultConfigStore();
        cc.and(c);
        service.querys("crm_user", cc);
    }
    /************************************************************************************************************
     *
     *                        请注意 不要被误导  init()中 只是举例说明sql的用法 只有复杂的SQL才需要这样实现
     *                        尽量用${key} #{key}的格式  与mybatis兼容
     *                        ${key} = ::key
     *                        #{key} = :key
     *                        正常情况下有可以通过suggest()中的方式实现查询
     *
     ************************************************************************************************************/
    @Test
    public void condition(){
        ConfigStore configs = new DefaultConfigStore();
        configs.and(Compare.GREAT, "ID", "1");
        configs.and(Compare.LESS, "ID", "5");
        String sql = "SELECT * FROM CRM_USER";
        service.querys(sql, configs);
        // SELECT * FROM CRM_USER WHERE ID > 1 AND ID < 5
    }
    @Test
    public void sql(){

    }
    @Test
    public void over(){
        String sql = "SELECT * FROM CRM_USER";
        ConfigStore configs = new DefaultConfigStore();
        configs.and(Compare.GREAT, "ID", "1");
        configs.and(Compare.LESS, "ID", "5", true, true);
        //相同列，但比较符不同所以不覆盖
        service.querys(sql, configs);
        // SELECT * FROM CRM_USER WHERE ID > 1 AND ID < 5

        configs = new DefaultConfigStore();
        configs.and(Compare.GREAT, "ID", "1");
        configs.and(Compare.GREAT, "ID", "5", true, true);
        //相同列，相同比较符的可以覆盖
        service.querys(sql, configs);
        // SELECT * FROM CRM_USER WHERE ID > 5


        configs = new DefaultConfigStore();
        configs.and(Compare.GREAT, "ID", "1");
        configs.and(Compare.GREAT, "ID", "5", true, false);
        //相同列，相同比较符的可以覆盖,值没有覆盖生成了数组[1,5] 但GREAT只能接收一个值
        service.querys(sql, configs);
        // SELECT * FROM CRM_USER WHERE ID > 1


        configs = new DefaultConfigStore();
        configs.and(Compare.IN, "ID", "1");
        configs.and(Compare.IN, "ID", "5", true, false);
        //相同列，相同比较符的可以覆盖,值没有覆盖生成了数组[1,5]  IN可以接收多个值
        service.querys(sql, configs);
        // SELECT * FROM CRM_USER WHERE ID IN(1,5)

    }

    @Test
    public void batch() {
        String sql = "UPDATE CRM_USER SET CODE = ? WHERE ID = ?";
        List values = new ArrayList<>();
        for(int i=0; i<100; i++){
            values.add(i);
            values.add(i);
        }
        service.execute(10,2,  sql, values);
    }
    @Test
    public void jdbc() {
        String sql = "SELECT * FROM CRM_USER WHERE ID = ? AND CODE > ?";
        ConfigStore configs = new DefaultConfigStore();
        configs.params(1,3);
        service.querys(sql, configs);
        sql = "UPDATE CRM_USER SET CODE = ? WHERE ID = ?";
        service.execute(sql, configs);
    }
    @Test
    public void pk(){
        DataSet set = service.querys("CRM_USER");
        LinkedHashMap<String, Column> cols = set.getMetadatas();
        for(Column column:cols.values()){
            //不检测是否主键
            System.out.println(column.getName()+":"+column.getComment() + ":" +column.isPrimaryKey());
        }
        ConfigTable.IS_AUTO_CHECK_METADATA = true;
        set = service.querys("CRM_USER");
        cols = set.getMetadatas();
        for(Column column:cols.values()){
            //开启IS_AUTO_CHECK_METADATA后 会检测详细的列信息
            System.out.println(column.getName()+":"+column.getComment() + ":" +column.isPrimaryKey());
        }
        LinkedHashMap<String,Column> pks = set.getPrimaryColumns();
        System.out.println(pks);
        for(DataRow row:set){
            System.out.println(row.getPrimaryColumns());
        }
    }
    @Test
    public void exists(){
        ConfigTable.IS_PRINT_EXCEPTION_STACK_TRACE = true;
        service.metadata().exists(new Table("schema","tablename"));
    }
    @Test
    public void likes(){
        ConfigStore configs = new DefaultConfigStore();
        configs.likes("Z");
        ServiceProxy.service().query("CRM_USER",configs);
    }
    @Test
    public void init() throws Exception{
        Table table = service.metadata().table("CRM_USER", false);
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("CRM_USER");
        table.addColumn("ID", "BIGINT").autoIncrement(true).primary(true);
        table.addColumn("CODE","int");
        service.ddl().create(table);
        DataRow row = new DataRow();
        row.put("CODE",1);
        service.insert("CRM_USER", row);
        service.query("CRM_USER", "++ID:");
        service.exists("CRM_USER", "++ID:");
        ConfigStore conditions = new DefaultConfigStore();
        //详细的查询条件构造方式 参考 anyline-simple-data-condition
        ConfigTable.IS_AUTO_CHECK_METADATA = true;
        String sql = "";
        // #{} 表示占位符  ${}直接替换
        conditions.param("CODES","1,2,3".split(","));
        sql = "SELECT * FROM CRM_USER WHERE 1=1 AND CODE='in:1' AND id in (SELECT id from CRM_USER WHERE CODE in (:CODES))";
        service.query(sql, conditions);
        //SELECT * FROM CRM_USER WHERE id in (SELECT id from CRM_USER WHERE CODE in (1,2,3)) LIMIT 0,1

        sql = "SELECT * FROM CRM_USER WHERE 1=1 AND id in (SELECT id from CRM_USER WHERE CODE in (${CODES}))";
        service.query(sql, conditions);
        //SELECT * FROM CRM_USER WHERE id in (SELECT id from CRM_USER WHERE CODE in (1,2,3)) LIMIT 0,1

        sql = "SELECT * FROM CRM_USER WHERE 1=1 AND id in (SELECT id from CRM_USER WHERE CODE in(#{CODES}))";
        service.query(sql, conditions);
        //SELECT * FROM CRM_USER WHERE id in (SELECT id from CRM_USER WHERE CODE in(?,?,?)) LIMIT 0,1

        ConfigStore configs = new DefaultConfigStore();
        configs.and(Compare.GREAT, "ID", "1");
        configs.and(Compare.LESS, "ID", "5");
        sql = "SELECT * FROM CRM_USER";
        service.querys(sql, configs);
        // SELECT * FROM CRM_USER WHERE ID > 1 AND ID < 5

        DataRow user = service.query("CRM_USER", configs);
        // SELECT * FROM CRM_USER WHERE ID > 1 AND ID < 5

        service.update(user, configs);
        // UPDATE CRM_USER SET ... WHERE ID > 1 AND ID < 5

        conditions = new DefaultConfigStore();
        sql = "SELECT * FROM CRM_USER WHERE CODE = :CODE";
        conditions.and("CODE", "100");
        service.querys(sql, conditions);
        //SELECT * FROM CRM_USER WHERE CODE = ?

        sql = "SELECT * FROM CRM_USER WHERE ID IN(:IDS)";
        conditions.param("IDS", "1,2,3".split(","));
        service.querys(sql, conditions);
        //SELECT * FROM CRM_USER WHERE CODE = ? AND ID IN(?,?,?)

        //:PARAM_CODE 与 {PARAM_CODE} 效果一致但不能混用
        //会生成占位符 "PARAM_CODE:100" 与SQL中的占位符能匹配成功 会把值100赋值给占位符
        sql = "SELECT * FROM CRM_USER WHERE CODE = :PARAM_CODE AND NAME != :PARAM_CODE AND FIND_IN_SET(:PARAM_CODE, CODE)";
        service.querys(sql, conditions);
        service.querys(sql, "PARAM_CODE:111");
        //SELECT * FROM CRM_USER WHERE CODE = ? AND NAME != ? AND FIND_IN_SET(?, CODE)

        sql = "SELECT * FROM CRM_USER WHERE CODE = #{PARAM_CODE}";
        service.querys(sql, "PARAM_CODE:100");
        //生成SQL SELECT * FROM CRM_USER WHERE CODE = ?


        //::PARAM_CODE 与 ${PARAM_CODE} 效果一致但不能混用
        //不生成占位符,而是在原sql上replace
        //在一些比较复杂的情况,简单占位符胜任不了时 会用到
        sql = "SELECT * FROM CRM_USER WHERE CODE = ::PARAM_CODE";
        service.querys(sql, "PARAM_CODE:100");
        //生成SQL SELECT * FROM CRM_USER WHERE CODE = 100


        sql = "SELECT * FROM CRM_USER WHERE CODE = ${PARAM_CODE}";
        service.querys(sql, "PARAM_CODE:100");
        //生成SQL SELECT * FROM CRM_USER WHERE CODE = 100


        //特别注意这以下情况 ID:1与SQL中的变量匹配不成功时，SQL会追加一个条件  ID = 1
        sql = "SELECT * FROM CRM_USER WHERE CODE = :PARAM_CODE";
        service.querys(sql, "PARAM_CODE:1", "ID:1");
        //生成SQL SELECT * FROM CRM_USER WHERE CODE = ? AND ID = ?


        //相当于web环境中的ConfigStore configs = condition()
        configs = new DefaultConfigStore();
        Map<String,Object> map = new HashMap<>();
        map.put("ID", "100");
        configs.setValue(map); //这里相当于接收request的提交的参数
        service.querys(sql, configs);

        configs = new DefaultConfigStore();
        configs.param("PARAM_CODE", "9");
        configs.param("TYPE_CODE", "100"); //param 如果没有匹配到参数则忽略，而不会添加新的查询条件
        configs.and("NAME", "zh");         //and 如果没有匹配到参数 会添加新的查询条件
        service.querys(sql, configs);


        //如果没有提供参数值 会生成 = NULL, 这种情况明显不符合预期
        sql = "SELECT * FROM CRM_USER WHERE CODE = :PARAM_CODE";
        service.querys(sql);
        //生成SQL  SELECT * FROM CRM_USER WHERE CODE = NULL

        sql = "UPDATE CRM_USER SET CODE = :CODE WHERE ID = :ID";
        service.execute(sql,"CODE:C001", "ID:1");
        //生成SQL  UPDATE  CRM_USER WHERE CODE = ? WHERE ID = ?


        sql = "UPDATE CRM_USER SET CODE = :CODE WHERE ID = :ID";
        service.execute(sql,"CODE:C001", "++ID:");
        //如果ID没有提供参数值，则整个SQL不执行

    }
    @Test
    public void replace() throws Exception{
        String SQL_PARAM_VARIABLE_REGEX = "(\\S+)\\s*\\(?(\\s*:+\\w+)(\\s|'|\\)|%|\\,)?";
        //(\S+)\s*\(?(\s*:+\w+)(\s|'|\)|%|\,)?
        String sql = null;
        sql = "UPDATE ::TABLE SET CODE = :CODE WHERE ID IN( ::IDS)";
        // UPDATE ::TABLE , UPDATE, ::TABLE , " "
        // IN(::IDS), IN(:, :IDS, )
        List<List<String>>  keys = RegularUtil.fetchs(sql, RunPrepare.SQL_VAR_PLACEHOLDER_REGEX_EXT, Regular.MATCH_MODE.CONTAIN);
        List<String> ids = new ArrayList<>();
        ids.add("1");
        ids.add("2");
        service.execute(sql, service.condition()
                .param("TABLE", "CRM_USER")
                .param("CODE","1")
                .param("IDS", ids) );
        sql = "SELECT * FROM CRM_USER WHERE ID IN(::ids)";
        keys = RegularUtil.fetchs(sql, RunPrepare.SQL_VAR_PLACEHOLDER_REGEX_EXT, Regular.MATCH_MODE.CONTAIN);
        service.querys(sql, new DefaultConfigStore().param("ids", "1,2,3"));
    }
    @Test
    public void test_sql(){
        String sql = "select res.* from( select ROW_NUMBER()\n" +
                "       over (ORDER bY round(cast(t1.sleep_count as numeric) / cast(t.bank_total as numeric), 2) desc, t1.sleep_count desc) num,\n" +
                "       t.*,\n" +
                "       t1.sleep_count,\n" +
                "       round(cast (t1.sleep_count as numeric) / cast(t.bank_total as numeric), 2) * 100 || '%'                              sleep_rate\n" +
                "from (select e.code cust_no, e.enterprise_name cust_name, count(1) bank_total\n" +
                "      from front_end_bank_interface f\n" +
                "               left join enterprise e\n" +
                "                         on f.enterprise_id = e.id\n" +
                "      where e.code is not null\n" +
                "        and e.enterprise_name is not null\n" +
                "        and e.status = 0\n" +
                "        and f.status = 0\n" +
                "      group by e.code, e.enterprise_name) t\n" +
                "         inner join (select e.code cust_no, e.enterprise_name cust_name, count(1) sleep_count\n" +
                "                     from front_end_bank_interface f\n" +
                "                              left join enterprise e\n" +
                "                                        on f.enterprise_id = e.id\n" +
                "                     where un_traded_status = 'Y'\n" +
                "                       and e.code is not null\n" +
                "                       and e.enterprise_name is not null\n" +
                "                       and e.status = 0\n" +
                "                       and f.status = 0\n" +
                "                     group by e.code, e.enterprise_name) t1\n" +
                "                    on t.cust_no = t1.cust_no\n" +
                "where t.cust_no not in (select cust_no from yql_cust_trans where cust_type = 'unordered') \n" +
                "order by round(cast(t1.sleep_count as numeric) /cast(t.bank_total as numeric), 2) desc, t1.sleep_count desc\n" +
                "LIMIT :page_size OFFSET :page_no - 1 * :page_size1 ) res";
        sql = " SELECT * FROM CRM_USER LIMIT :page_size OFFSET (:page_no - 1) * :page_size  ";
        ConfigStore configs = new DefaultConfigStore();
        configs.param("page_size",1);
        configs.param("page_size1",11);
        configs.param("page_no",2);
        service.querys(sql, configs);
    }

    public static void main(String[] args) throws Exception{
        String SQL_PARAM_VARIABLE_REGEX = "(\\S+)\\s*\\(?(\\s*:+\\w+)(\\s|'|\\)|%|\\,)?";
        // 占位符前面的标识,中间可能有空格,占位符以字母开头(避免CODE='A:1'但避免不了'A:A1'这时应该换成$#),后面可能是 ' ) % ,
        SQL_PARAM_VARIABLE_REGEX = "(\\S+)\\s*\\(?(\\s*:+[A-Za-z]\\w+)(\\s|'|\\)|%|\\,)?";
        String text = " SELECT * FROM CRM_USER LIMIT WHERE CODE = 'A:B1' LIMIT :page_size OFFSET (:page_no - 1) * :page_size1  ";
        List<List<String>> keys = RegularUtil.fetchs(text, SQL_PARAM_VARIABLE_REGEX, Regular.MATCH_MODE.CONTAIN);
        for(List<String> line:keys){
            System.out.println(line);
        }
    }
    @Test
    public void test(){
        String sql = "SELECT * FROM CRM_USER AS M LEFT JOIN CRM_USER AS F ON M.ID = F.ID";
        ConfigStore configs = new DefaultConfigStore();
        configs.and(Compare.NOT_EQUAL, "M", "ID", "0");
        configs.or(Compare.GREAT, "M.ID","2");
        service.querys("CRM_USER", configs);
        service.querys(sql, configs);
    }
    /********************************************************************
     *
     *                        init()中的SQL 最好应该这样写
     *
     **************************************************************/
    public void suggest(){

        String value = "100";
        service.query("CRM_USER", "CODE:"+value);
        //生成SQL SELECT * FROM CRM_USER WHERE CODE = ?

        //默认情况下如果value没有值则不会拼接相应的查询条件
        value = null;
        service.query("CRM_USER", "CODE:"+value);
        value =  "";
        service.query("CRM_USER", "CODE:"+value);
        //生成SQL SELECT * FROM CRM_USER

        //如果希望没有提供参数值是不执行SQL
        value = null;
        service.query("CRM_USER", "++CODE:"+value);
        value =  "";
        service.query("CRM_USER", "++CODE:"+value);
        //不会生成SQL 返回new DataSet();

        //如果希望没有提供参数值是依然拼接这个查询条件
        value = null;
        service.query("CRM_USER", "+CODE:"+value);
        value =  "";
        service.query("CRM_USER", "+CODE:"+value);
        //生成SQL SELECT * FROM CRM_USER WHERE CODE IS NULL


        DataRow row = new DataRow();
        row.put("NAME","中文名");
        row.put("CODE", "C001");
        row.setPrimaryKey("CODE");  //临时设置主键，执行update时根据设置的主键执行,否则按默认主键

        service.save("CRM_USER", row);
    }


}
