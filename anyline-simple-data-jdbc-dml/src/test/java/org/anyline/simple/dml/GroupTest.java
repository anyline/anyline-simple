package org.anyline.simple.dml;

import org.anyline.data.param.Config;
import org.anyline.data.param.ConfigBuilder;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.TableBuilder;
import org.anyline.data.param.init.DefaultConfig;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.data.prepare.RunPrepare;
import org.anyline.entity.*;
import org.anyline.entity.generator.GeneratorConfig;
import org.anyline.entity.generator.PrimaryGenerator;
import org.anyline.metadata.Table;
import org.anyline.proxy.ServiceProxy;
import org.anyline.proxy.TransactionProxy;
import org.anyline.service.AnylineService;
import org.anyline.simple.dml.entity.User;
import org.anyline.util.BeanUtil;
import org.anyline.util.ConfigTable;
import org.anyline.util.regular.Regular;
import org.anyline.util.regular.RegularUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
public class GroupTest {
    private Logger log = LoggerFactory.getLogger(GroupTest.class);

    @Autowired
    private AnylineService service          ;


    @Test
    public void sql() throws Exception{
        String sql = "SELECT MAX(ID) AS MAX_ID , CODE FROM CRM_USER";
        for(int i=0; i<10; i++) {
            ServiceProxy.querys(sql, new DefaultConfigStore().group("CODE")
                    .order("CODE").having("COUNT(*)>1"));
        }
    }
    @Test
    public void having() throws Exception{
        RunPrepare prepare = TableBuilder.init("crm_user(max(id) as max_id, code)").build();
        ConfigStore configs = new DefaultConfigStore();
        ConfigStore having = new DefaultConfigStore();
        having.and("MAX(ID)>0").or("MIN(ID)<0").and(Compare.GREAT, "COUNT(*)", 1);
        configs.having(having);
        configs.group("CODE");
        configs.and("ID > 0");
        service.querys(prepare, configs);
        String json = configs.json();
        System.out.println(json);
        configs = ConfigBuilder.build(json);
        service.querys(prepare, configs);
        json = configs.json();
        System.out.println(json);
    }

}
