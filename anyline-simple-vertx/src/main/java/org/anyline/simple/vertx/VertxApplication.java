package org.anyline.simple.vertx;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;

public class VertxApplication {
    public static void main(String[] args) {
        HttpServer server = Vertx.vertx().createHttpServer();
        Handler<HttpServerRequest> handler = new Handler<HttpServerRequest>() {
            @Override
            public void handle(HttpServerRequest event) {
                HttpServerResponse response = event.response();
                response.putHeader("content-type", "text/plain");
                response.end("Hello World!");
            }
        };

        server.requestHandler(handler);
        server.listen(8081);
    }
}
