package org.anyline.simple.clickhouse;

import org.anyline.metadata.Database;
import org.anyline.proxy.ServiceProxy;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedHashMap;

@SpringBootTest
public class MetadataTest {
    @Test
    public void test() throws Exception {

        LinkedHashMap<String, Database> databases = ServiceProxy.metadata().databases();
        System.out.println(databases);

        Database database = ServiceProxy.metadata().database("default");
        System.out.println(database);

        database = ServiceProxy.metadata().database("simple");
        if(null == database){
            database = new Database("simple");
            ServiceProxy.ddl().create(database);
        }
    }
}
