package org.anyline.simple.net;

import org.anyline.entity.DataRow;
import org.anyline.net.HttpUtil;
import org.anyline.util.BasicUtil;
import org.anyline.util.BeanUtil;
import org.apache.http.entity.StringEntity;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class NetTest {



    /*http://192.168.110.194:9696/ConvertService.ashx
    {
        "async": false, //是否异常
        "filetype": "docx",
        "key": "Khirz6zTPdfd7", //随机数 用来生成输出url
        "outputtype": "pdf", //输出格式
        "title": "标题.pdf", //有没有后缀都可以
        "url": "http://192.168.110.42/hj-laboratory/2023/10/16/%E6%B5%B7%E6%A3%80%E7%BB%93%E7%AE%97%E5%8D%95%EF%BC%88word%EF%BC%89%E7%89%88_20231016111108A018.docx"
    }

    <?xml version="1.0" encoding="utf-8"?>
    <FileResult>
        <FileUrl>http://192.168.110.194:9696/cache/files/data/conv_Khirz6zTPdfd7_pdf/output.pdf/example.pdf?md5=P07Fq9MCnco2qCjV2c3ZMg&amp;expires=1712129715&amp;filename=example.pdf</FileUrl>
        <FileType>pdf</FileType>
        <Percent>100</Percent>
        <EndConvert>True</EndConvert>
    </FileResult>
    */
    @Test
    public void onlyoffice() throws UnsupportedEncodingException {
        String url = "http://192.168.110.42/hj-laboratory/2023/10/16/海检结算单（word）版_20231016111108A018.docx";
        url = HttpUtil.encode(url, true, true);

        String server = "http://192.168.110.194:9696/web-apps/apps";
        String title = "转换后标题.pdf"; //有没有后缀都可以
        String pdf_server = HttpUtil.host(server);
        String pdf_api = HttpUtil.mergePath(pdf_server, "ConvertService.ashx");

        Map<String, Object> params = new HashMap<>();
        params.put("async", false);
        params.put("filetype", "docx");
        params.put("outputtype", "pdf");
        params.put("title", title);
        params.put("key", BasicUtil.getRandomNumberString(16));
        params.put("url", url);
        String xml = HttpUtil.post(pdf_api, "UTF-8", new StringEntity(BeanUtil.map2json(params), "UTF-8")).getText();
        DataRow row = DataRow.parseXml(xml);
        System.out.println(row);
    }

}
