package org.anyline.simple.ds;

import org.anyline.data.datasource.DataSourceHolder;
import org.anyline.proxy.ServiceProxy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("org.anyline")
public class DDLApplication {


	public static void main(String[] args){
		SpringApplication application = new SpringApplication(DDLApplication.class);
		application.run(args);


			//doris因为与mysql使用同样的协议并且接口返回标识一样，所以识别不出来，需要在url上指定adapter
			try {
				String url = "jdbc:mysql://localhost:33306/simple_sso?useUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true";
				DataSourceHolder.reg("sso", "com.zaxxer.hikari.HikariDataSource", "com.mysql.cj.jdbc.Driver", url, "root", "root");

				url = "jdbc:mysql://localhost:9030/simple?useUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true&adapter=doris";
				DataSourceHolder.reg("ds", "com.zaxxer.hikari.HikariDataSource", "com.mysql.cj.jdbc.Driver", url, "root", "");
				ServiceProxy.service("ds").tables();
			}catch (Exception e){
				e.printStackTrace();
			}


	}

}
