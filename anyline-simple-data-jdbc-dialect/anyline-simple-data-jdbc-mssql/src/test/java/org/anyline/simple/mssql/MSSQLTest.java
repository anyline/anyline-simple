package org.anyline.simple.mssql;

import org.anyline.data.jdbc.adapter.JDBCAdapter;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.entity.*;
import org.anyline.metadata.Catalog;
import org.anyline.metadata.Column;
import org.anyline.metadata.Schema;
import org.anyline.metadata.Table;
import org.anyline.proxy.ServiceProxy;
import org.anyline.service.AnylineService;
import org.anyline.util.BasicUtil;
import org.anyline.util.ConfigTable;
import org.anyline.util.LogUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@SpringBootTest
public class MSSQLTest {
    private Logger log = LoggerFactory.getLogger(MSSQLTest.class);
    @Lazy
    @Autowired
    private AnylineService service          ;
    @Autowired
    private JdbcTemplate jdbc               ;
    private Catalog catalog = new Catalog("simple")      ; // 可以相当于数据库名
    private Schema schema   = null          ; // 如 dbo
    private String table    = "CRM_USER"    ; // 表名
    @Test
    public void unicode() throws Exception{
        Table table = service.metadata().table("USER_UNICODE");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("USER_UNICODE");
        table.addColumn("ID", "INT").setAutoIncrement(true);
        table.addColumn("AGE", "INT");
        table.addColumn("CODE", "NVARCHAR(10)").setCharset("Latin1_General_100_CI_AS_SC");
        service.ddl().create(table);
        DataRow row = new DataRow();
        row.put("CODE", "A");
        row.put("AGE", 20);
        ConfigStore configs = new DefaultConfigStore();
        configs.setPlaceholder(false);
        configs.setUnicode(true);
        service.insert(table, row, configs);
    }
    @Test
    public void id(){
        String sql ="SET IDENTITY_INSERT [simple].[dbo].CRM_USER ON";
        service.execute(sql);
    }
    @Test
    public void bit() throws Exception {
        Table tab = new Table("abit");
        tab.addColumn("ID","INT");
        tab.addColumn("is_user", "bit").setDefaultValue(0);
        service.ddl().create(tab);

    }
    @Test
    public void version() throws SQLException {
        String name = jdbc.getDataSource().getConnection().getMetaData().getDatabaseProductName();
        String version = jdbc.getDataSource().getConnection().getMetaData().getDatabaseProductVersion();
        String catalog = jdbc.getDataSource().getConnection().getCatalog();
        String schema = jdbc.getDataSource().getConnection().getSchema();
        log.warn("\nname:{}\nversion:{}\ncatalog:{}\nschema:{}",name, version, catalog, schema);
    }

    @Test
    public void info() {
        log.warn("\ntype:{}\ncatalog:{}\nschema:{}\ndatabase:{}\nproduct:{}\nversion:{}"
                ,service.metadata().type()
                ,service.metadata().catalog()
                ,service.metadata().schema()
                ,service.metadata().database()
                ,service.metadata().product()
                ,service.metadata().version()
        );
    }

    @Test
    public void autoIncrement() throws Exception{
        Table table = service.metadata().table("TAB_AUTOINCREMENT");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("TAB_AUTOINCREMENT");
        table.addColumn("ID1","BIGINT").setAutoIncrement(true).setComment("主键");
       // table.addColumn("ID2","BIGINT").setAutoIncrement(true);
        service.ddl().create(table);

    }


    @Test
    public void tabQuery() throws Exception{
        Table table = service.metadata().table("crm_user", false);
        if(null == table){
            table = new Table("crm_user");
            table.addColumn("ID","INT");
            service.ddl().create(table);
        }
        List<Table> tables = service.metadata().tables(false, new Catalog("SIMPLE"), new Schema(null), "CRM_USER",1, false);
        System.out.println(tables);
    }
    @Test
    public void comment() throws Exception{
        Table table = service.metadata().table("TAB_AUTOINCREMENT");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("TAB_AUTOINCREMENT");
        table.addColumn("ID1","BIGINT").setAutoIncrement(true).setComment("主键");
        Catalog catalog = new Catalog();
        table.setCatalog(catalog);
        Schema schema = new Schema();
        table.setSchema(schema);
        service.ddl().create(table);
    }
    @Test
    public void metadata(){
        ConfigTable.IS_THROW_SQL_QUERY_EXCEPTION = true;
        ConfigTable.IS_PRINT_EXCEPTION_STACK_TRACE = true;
        // 1直接获取元数据
        //sql server 列元数据中不返回表名，所以查不到注释
        LinkedHashMap<String,Column> columns = service.metadata("SELECT ID AS USER_ID, ID AS ID, CODE AS USER_CODE FROM CRM_USER WHERE ID = :ID ");
        for(Column column:columns.values()){
            System.out.println(column);
        }
        //2.获取表结构的同时获取列数据
        Table tab = service.metadata().table("CRM_USER");
        columns = tab.getColumns();
        for(Column column:columns.values()){
            System.out.println(column);
        }
        //3.通过查询结果获取元数据
        DataSet set = service.querys("SELECT F.ID AS '用户编号', M.ID AS ID, M.CODE AS USER_CODE FROM CRM_USER AS M LEFT JOIN CRM_USER AS F ON M.ID = F.ID");
        columns = set.getMetadatas();
        for(Column column:columns.values()){
            System.out.println(column);
        }
    }
    @Test
    public void test() throws Exception {
        //默认不查表结构
        Table test = new Table();
        test.setName("test_"+System.currentTimeMillis());
        test.addColumn("ID", "int");
        service.ddl().create(test);
        LinkedHashMap<String, Table> tables = service.metadata().tables();
        for(Table table:tables.values()){
            System.out.println(table);
        }
        List<Table> list = service.metadata().tables(true);
        for(Table item:list){
            System.out.println(item);
        }
        //strut = true时才查表结构
        list = service.metadata().tables(false,true);

        for(Table item:list){
            System.out.println(item);
            System.out.println(item.getColumns().size());
        }
        //查单个表时默认查表结构
        Table table = service.metadata().table("CRM_USER");
        //如果不需要查结构
        table = service.metadata().table("CRM_USER", false);
    }

    @Test
    public void ddl() throws Exception{
        ConfigTable.IS_THROW_SQL_UPDATE_EXCEPTION = true; //遇到SQL异常直接抛出
        //检测表结构
        Table table = service.metadata().table(catalog, schema, this.table);
        //如果存在则删除
        if(null != table){
            service.ddl().drop(table);
        }

        //再查询一次
        table = service.metadata().table(catalog, schema, this.table);
        Assertions.assertNull(table);

        //定义表结构
        table = new Table(catalog, schema, this.table);

        //添加列
        //自增长列 如果要适配多种数据库 autoIncrement 有必须的话可以设置起始值与增量 autoIncrement(int seed, int step)
        table.addColumn("ID", "INT", false, null).setComment("主键").autoIncrement(true).primary(true);
        table.addColumn("CODE", "VARCHAR(50)").setComment("编号");
        table.addColumn("NAME", "VARCHAR(50)").setComment("名称");
        //默认当前时间 如果要适配多种数据库 用 SQL_BUILD_IN_VALUE.CURRENT_DATETIME
        table.addColumn("REG_TIME", "datetime").setComment("注册时间").setDefaultValue(JDBCAdapter.SQL_BUILD_IN_VALUE.CURRENT_DATETIME);
        table.addColumn("DATA_VERSION", "double", false, 1.1).setComment("数据版本");
        table.addColumn("bt","varbinary(8000)");//最大8000

        //创建表
        service.ddl().create(table);

        //再查询一次
        table = service.metadata().table(catalog, schema, this.table);
        Assertions.assertNotNull(table);

        table.getColumn("NAME").setType("INT").setPrecision(10);
        service.ddl().save(table);
    }

    @Test
    public void max(){
        Table table = service.metadata().table("TTT");
        System.out.println(table);
    }
    @Test
    public void tables(){
        service.metadata().tables();
    }
    @Test
    public void page(){
        DataSet set = new DataSet();
        for(int i=0; i<100; i++){
            set.add().put("CODE", i);
        }
        service.insert(table, set);
        service.querys(table, new DefaultConfigStore().page(2,3).order("CODE"));
    }
    @Test
    public void page2(){
        service.querys(table+"<CODE,ID>", new DefaultConfigStore().page(2,3));
    }
    @Test
    public void dml() throws Exception{
        Table t = service.metadata().table(table);
        DataSet set = new DataSet();
        for(int i=1; i<10; i++){
            DataRow row = new DataRow();
            //只插入NAME  ID自动生成 REG_TIME 默认当时时间
            row.put("NAME", "N"+i);
            set.add(row);
        }
        long qty = service.insert(100, table, set);
        log.warn(LogUtil.format("[批量插入][影响行数:{}][生成主键:{}]", 36), qty, set.getStrings("ID"));
        Assertions.assertEquals(qty , set.size());

        DataRow row = new DataRow();
        row.put("NAME", "N");
        //当前时间，如果要适配多种数据库环境尽量用SQL_BUILD_IN_VALUE,如果数据库明确可以写以根据不同数据库写成: row.put("REG_TIME","${now()}"); sysdate,getdate()等等
        row.put("REG_TIME", JDBCAdapter.SQL_BUILD_IN_VALUE.CURRENT_DATETIME);
        qty = service.insert(table, row);
        log.warn(LogUtil.format("[单行插入][影响行数:{}][生成主键:{}]", 36), qty, row.getId());
        Assertions.assertEquals(qty , 1);
        Assertions.assertNotNull(row.getId());

        //查询全部数据
        set = service.querys(table);
        log.warn(LogUtil.format("[query result][查询数量:{}]", 36), set.size());
        log.warn("[多行查询数据]:{}",set.toJSON());
        Assertions.assertEquals(set.size() , 10);

        //只查一行
        row = service.query(table);
        log.warn("[单行查询数据]:{}",row.toJSON());
        Assertions.assertNotNull(row);
        Assertions.assertEquals(row.getId(), "1");

        //查最后一行
        row = service.query(table, "ORDER BY ID DESC");
        log.warn("[单行查询数据]:{}",row.toJSON());
        Assertions.assertNotNull(row);
        Assertions.assertEquals(row.getInt("ID",10), 10);

        //更新
        //put覆盖了Map.put返回Object
        row.put("NAME", "SAVE NAME");

        //set由DataRow声明实现返回DataRow可以链式操作
        row.set("CODE", "SAVE CODE").set("DATA_VERSION", 1.2);

        //save根据是否有主键来判断insert | update
        //可以指定SAVE哪一列
        service.save(row, "NAME");
        service.save(row);
        row.put("NAME", "UPDATE NAME");

        /*
         * 注意这里的page一般不手工创建，而是通过AnylineController中的condition自动构造
         * service.querys("CRM_USER", condition(true, "ID:id","NAME:%name%", TYPE_CODE:[type]), "AGE:>=age");
         * true:表示分页 或者提供int 表示每页多少行
         * ID:表示数据表中的列
         * id:表示http提交的参数名
         * [type]:表示数组
         * */

        //分页查询
        //每页3行,当前第2页(下标从1开始)
        PageNavi page = new DefaultPageNavi(2, 3);

        //无论是否分页 都返回相同结构的DataSet
        set = service.querys(table, page);
        log.warn(LogUtil.format("[分页查询][共{}行 第{}/{}页]", 36), page.getTotalRow(), page.getCurPage(), page.getTotalPage());
        log.warn(set.toJSON());
        Assertions.assertEquals(page.getTotalPage() , 4);
        Assertions.assertEquals(page.getTotalRow() , 10);


        //模糊查询
        set = service.querys("CRM_USER", "NAME:%N%");
        log.warn(LogUtil.format("[模糊查询][result:{}]", 36), set.toJSON());
        set = service.querys("CRM_USER", "NAME:%N");
        log.warn(LogUtil.format("[模糊查询][result:{}]", 36), set.toJSON());
        set = service.querys("CRM_USER", "NAME:N%");
        log.warn(LogUtil.format("[模糊查询][result:{}]", 36), set.toJSON());

        //其他条件查询
        //in
        List<Integer> in = new ArrayList<>();
        in.add(1);
        in.add(2);
        in.add(3);
        ConfigStore condition = new DefaultConfigStore();
        condition.ands("ID", in);

        //not in
        condition.and(Compare.NOT_IN, "NAME", "N1");
        List<Integer> notin = new ArrayList<>();
        notin.add(10);
        notin.add(20);
        notin.add(30);
        condition.and(Compare.NOT_IN, "ID", notin);

        //between
        List<Integer> between = new ArrayList<>();
        between.add(1);
        between.add(10);
        condition.and(Compare.BETWEEN, "ID", between);

        // >=
        condition.and(Compare.GREAT_EQUAL, "ID", "1");

        //前缀
        condition.and(Compare.LIKE_PREFIX, "NAME", "N");

        set = service.querys("CRM_USER", condition);
        log.warn(LogUtil.format("[后台构建查询条件][result:{}]", 36), set.toJSON());
        Assertions.assertEquals(set.size() , 2);

        qty = service.count(table);
        log.warn(LogUtil.format("[总数统计][count:{}]", 36), qty);
        Assertions.assertEquals(qty , 10);

        //根据默认主键ID更新
        row.put("CODE",1001);
        //默认情况下 更新过的列 会参与UPDATE
        qty = service.update(row);
        log.warn(LogUtil.format("[根据主键更新内容有变化的化][count:{}]", 36), qty);


        //根据临时主键更新,注意这里更改了主键后ID就成了非主键，但未显式指定更新ID的情况下,ID不参与UPDATE
        row.setPrimaryKey("NAME");
        qty = service.update(row);
        log.warn(LogUtil.format("[根据临时主键更新][count:{}]", 36), qty);

        //显示指定更新列的情况下才会更新主键与默认主键 ID列上有递增 不要修改
        qty = service.update(row,"NAME","CODE");
        log.warn(LogUtil.format("[更新指定列][count:{}]", 36), qty);

        //根据条件更新
        ConfigStore store = new DefaultConfigStore();
        store.and(Compare.GREAT, "ID", "1")
                
                .and(" CODE > 1")
                .and("NAME IS NOT NULL");
        qty = service.update(row, store);
        log.warn(LogUtil.format("[根据条件更新][count:{}]", 36), qty);


        qty = service.delete(set);
        log.warn("[根据ID删除集合][删除数量:{}]", qty);
        Assertions.assertEquals(qty, set.size());

        //根据主键删除
        qty = service.delete(row);
        log.warn("[根据ID删除][删除数量:{}]", qty);
        Assertions.assertEquals(qty, 1);

        set = service.querys(table, "ID:1");
        qty = service.delete(table, "ID","1");
        log.warn("[根据条件删除][删除数量:{}]", qty);
        Assertions.assertEquals(qty, set.size());

        set = service.querys(table, "ID IN(2,3)");
        qty = service.deletes(table, "ID","2","3");
        log.warn("[根据条件删除][删除数量:{}]", qty);
        Assertions.assertEquals(qty, set.size());
    }

    @Test
    public void help() throws Exception{
        Connection con = jdbc.getDataSource().getConnection();
        System.out.println("\n--------------[metadata]------------------------");
        System.out.println("catalog:"+con.getCatalog());
        System.out.println("schema:"+con.getSchema());
        ResultSet set = con.getMetaData().getTables(null, null, table, "TABLE".split(","));
        ResultSetMetaData md = set.getMetaData();
        if (set.next()) {
            System.out.println("\n--------------[table metadata]------------------------");
            for (int i = 1; i <= md.getColumnCount(); i++) {
                String column = md.getColumnName(i);
                System.out.print(BasicUtil.fillRChar(column, " ",20) + " = ");
                Object value = set.getObject(i);
                System.out.println(value);
            }
        }
        set = jdbc.getDataSource().getConnection().getMetaData().getColumns(null, null, null, null);
        md = set.getMetaData();
        if (set.next()) {
            System.out.println("\n--------------[column metadata]------------------------");
            for (int i = 1; i <= md.getColumnCount(); i++) {
                String column = md.getColumnName(i);
                System.out.print(BasicUtil.fillRChar(column, " ",37) + " = ");
                Object value = set.getObject(i);
                System.out.println(value);
            }
        }

    }
}
