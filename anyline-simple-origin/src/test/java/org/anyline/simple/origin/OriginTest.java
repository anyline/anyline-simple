package org.anyline.simple.origin;

import org.anyline.adapter.init.DefaultEnvironmentWorker;
import org.anyline.data.transaction.TransactionDefine;
import org.anyline.data.transaction.TransactionState;
import org.anyline.data.transaction.init.DefaultTransactionDefine;
import org.anyline.entity.DataRow;
import org.anyline.proxy.ServiceProxy;
import org.anyline.proxy.TransactionProxy;
import org.anyline.util.ConfigTable;


public class OriginTest {
    public static void main(String[] args) throws Exception {
        DefaultEnvironmentWorker.start();
        ConfigTable.IS_PRINT_EXCEPTION_STACK_TRACE = true;
        master();
    }
    public static void master() throws Exception{
        //全局事务
        TransactionDefine define = new DefaultTransactionDefine();
        define.setName("tx1");//如果不设置会生成一个随机name
        define.setMode(TransactionDefine.MODE.APPLICATION);
        TransactionState state = TransactionProxy.start(define);
        DataRow row = new DataRow();
        row.put("SALARY", 1);
        row.put("SALARY_12", 1);
        long qty = ServiceProxy.insert("CRM_USER", row);
        System.out.println("影响行数:"+qty);
        ServiceProxy.query("CRM_USER");
        String name = state.getName();
        //第二个线程或会话 使用同一个name启动事务
        slave(name);
    }
    public static void slave(String name) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TransactionDefine define = new DefaultTransactionDefine();
                    define.setName(name);
                    define.setMode(TransactionDefine.MODE.APPLICATION);
                    TransactionProxy.start(define);

                    DataRow row = new DataRow();
                    row.put("CODE", 2);
                    ServiceProxy.insert("CRM_USER", row);
                }catch (Exception e){
                    e.printStackTrace();
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        ServiceProxy.query("CRM_USER");
                    }
                }).start();
            }
        }).start();
    }
}
