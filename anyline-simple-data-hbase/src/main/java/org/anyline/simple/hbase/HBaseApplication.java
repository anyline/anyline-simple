package org.anyline.simple.hbase;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HBaseApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(HBaseApplication.class);
        application.run(args);
    }

}
