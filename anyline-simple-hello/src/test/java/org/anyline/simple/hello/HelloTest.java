package org.anyline.simple.hello;

import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.entity.Compare;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.entity.OriginRow;
import org.anyline.metadata.Column;
import org.anyline.metadata.Table;
import org.anyline.service.AnylineService;
import org.anyline.util.ConfigTable;
import org.anyline.util.LogUtil;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@SpringBootTest
public class HelloTest {
    private Logger log = LoggerFactory.getLogger(HelloTest.class);
    @Autowired
    private AnylineService service;

    @Test
    public void logs() {
        DataSet set = service.querys("hr_employee");
        System.out.println(LogUtil.table(set));
    }
    @Test
    public void pl() {
        ConfigStore configs = new DefaultConfigStore();
        configs.param("CODE","C1");
        configs.param("ID", "1");
        DataSet set = service.querys("select * from hr_employee where id = ::ID AND CODE = '::CODE'", configs);
        System.out.println(LogUtil.table(set));
    }
    @Test
    public void logss() {
        ConfigTable.IS_LOG_QUERY_RESULT = true;
        DataSet set = service.querys("hr_employee");
    }
    @Test
    public void test(){
        ConfigStore configs = new DefaultConfigStore();
        configs.and("ID is not null");
        service.querys("crm_user", configs);

        //修改主键
        DataRow row = new DataRow();
        row.put("code", 1);
        row.put("name","zh");
        row.setPrimaryKey("code");
        service.save("crm_user", row);
        //UPDATE crm_user SET NAME = ? WHERE CODE = ?
    }

    @Test
    public void update1(){
        DataRow row = new OriginRow();
        System.out.println(row.getPrimaryKeys());
    }
    @Test
    public void init() throws Exception{

        //先创建个表
        Table table = service.metadata().table("crm_user");
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("crm_user");
        Column column = new Column("ID").autoIncrement(true).setType("int").primary(true);
        table.addColumn(column);
        table.addColumn("CODE","varchar(10)");
        table.addColumn("NAME","varchar(10)");
        table.addColumn("LNG","DECIMAL(10,7)");
        service.ddl().create(table);

        DataRow test = new DataRow();
        test.put("NAME",  "1,2,3".split(","));
        test.put("LNG", new BigDecimal("123.123"));
        service.save("crm_user", test);
        test = service.query("crm_user");
        System.out.println(test);

        //插入数据
        DataRow row = new DataRow();
        row.put("CODE","101");
        row.put("NAME","ZH");
        service.insert("crm_user", row);

        row.put("CODE","NULL");
        service.save("crm_user", row);
        row = service.query("crm_user(max(id) as id)");
        //查询数据
        DataSet set = service.querys("crm_user", "ID>1");
        System.out.println(set);
        set = service.querys("crm_user", service.condition().and("ID", "1,2,3".split(",")));
        System.out.println(set);
        //合计行数 与查询的参数一样只是返回值不一样
        long qty = service.count("crm_user", "id>1");
        System.out.println(qty);
        qty = service.count("crm_user(distinct id)", "id>1");
        System.out.println(qty);


        row = service.query("CRM_USER", "ID:1");
        row.put("CODE","102");
        //更新数据
        service.update(row);
    }

    /**
     * XML中自定义SQL
     */
    @Test
    public void xml(){
        DataSet users = service.querys("crm.user:USER_LIST");
        System.out.println(users);
    }
    @Test
    public void cols(){
        ConfigTable.IS_SQL_DELIMITER_OPEN = true;
        service.query("CRM_USER(id,code)");
    }
    @Test
    public void query() {
        //当前schema中没有的表 默认查不到
        Table table = service.metadata().table("art_comment");
        if (null != table) {
            System.out.println(table.getCatalog() + ":" + table.getSchema() + ":" + table.getName());
        }

        //当前schema中没有的表 greedy=rue 可以查到其他schema中的表
        table = service.metadata().table(true, "art_comment");
        if (null != table) {
            System.out.println(table.getName(true));
        }

        ConfigStore configs = new DefaultConfigStore();
        configs.and(Compare.GREAT, "ID", "1");
        configs.and(Compare.LESS, "ID", "5");
        DataSet users = service.querys("SELECT * FROM CRM_USER", configs);
        System.out.println(users);
        DataRow user = service.query("CRM_USER", configs);
        System.out.println(user);
        if (null != user) {
            service.update(user, configs);
        }

    }
    @Test
    public void table(){
        ConfigTable.IS_AUTO_CHECK_METADATA = true;
        ConfigStore configs = new DefaultConfigStore();
        configs.and(Compare.GREAT, "ID", "1");
        DataSet users = service.querys(new Table("simple","CRM_USER"), configs.limit(3));
        System.out.println(users);
        service.update(users, "CODE");
    }
    @Test
    public void update(){
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        service.execute("update crm_user set name = 1",
                new DefaultConfigStore().in("code", list));

        DataRow row = new DataRow();
        row.put("name", "Z");

        ConfigStore configs = new DefaultConfigStore();
        configs.in("CODE", list);
        service.update("crm_user", row, configs);
    }
    @Test
    public void metadata(){
        //根据sql获取列数据类型
        LinkedHashMap<String,Column> cols = service.metadata("SELECT * FROM CRM_USER WHERE 1=0");
        for(Column column:cols.values()){
            System.out.println(column);
        }
        ConfigTable.IS_CHECK_EMPTY_SET_METADATA = true;
        new DefaultConfigStore().IS_CHECK_EMPTY_SET_METADATA(true);
        DataSet set = service.querys("crm_user", "1=0");
        cols = set.getMetadatas();
        for(Column column:cols.values()){
            System.out.println(column);
        }
        Table tab = new Table("tt_"+System.currentTimeMillis());
        tab.setColumns(set.getMetadatas());
        try {
            service.ddl().save(tab);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    public void ognl() throws Exception{
        DataSet set = new DataSet();
        for(int i=0; i<10; i++){
            DataRow row = new DataRow();
            row.put("INDEX", i);
            row.put("CODE","C"+i);
            set.add(row);
        }
        set = set.select.ognl("INDEX>5 && INDEX<7");

        System.out.println(set);
    }
    @Test
    public void nullCondition() throws Exception{
        service.maps("select * from crm_user", new DefaultConfigStore().and(Compare.NULL,"ID"));
    }
    @Test
    public void index() throws Exception{
        Table table = service.metadata().table("a");
        System.out.println(table);

    }
}
