package org.anyline.simple.gbase8s;

import org.anyline.metadata.Column;
import org.anyline.metadata.Table;
import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.service.AnylineService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;



@SpringBootApplication
public class GBase8sApplication {
    private static AnylineService service;
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(GBase8sApplication.class);
        ConfigurableApplicationContext context = application.run(args);
        service = (AnylineService) context.getBean("anyline.service");
        init();
    }
    public static void init() {
        try {
            Table table = service.metadata().table("crm_user");
            if (null != table) {
                service.ddl().drop(table);
            }
            table = new Table("crm_user");
            Column column = new Column("ID").autoIncrement(true).setType("int").primary(true);
            table.addColumn(column);
            table.addColumn("CODE", "varchar(10)");
            table.addColumn("NAME", "varchar(10)");
            service.ddl().create(table);
        }catch (Exception e){
            e.printStackTrace();
        }
        DataRow row = new DataRow();
        row.put("ID", 1);
        row.put("NAME", "张三");
        service.insert("crm_user", row);
        DataSet set = service.querys("crm_user",1,1);
    }
}
