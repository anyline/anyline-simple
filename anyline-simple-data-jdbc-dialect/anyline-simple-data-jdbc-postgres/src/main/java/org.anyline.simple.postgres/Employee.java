package org.anyline.simple.postgres;

import org.anyline.entity.geometry.Point;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

@Table(name="HR_EMPLOYEE")
public class Employee extends BaseEntity{

    @Column(name = "type_array", columnDefinition = "text[]")
    private List<Integer> types; //兼容或可转换类型

    @Column(name = "double_array", columnDefinition = "int[]")
    private double[] double_array; //兼容或可转换类型

    @GeneratedValue(generator = "disable") //不在java中生成主键
    private Long id;
    @Column(name = "NAME", length = 10)
    private String nm;

    @Column(name = "CODE", length = 10)
    private String workCode;
    //对应date类型
    private LocalDate birthday;

    //如果属性上没有注解会 会根据 ConfigTable.ENTITY_FIELD_COLUMN_MAP 转换;
    //默认"camel_"属性小驼峰转下划线 joinYmd > join_ymd
    private String joinYmd;

    //这一列在数据库中没有
    @Transient
    private int age;

    private float salary;

    //对应数据库blob类型
    private byte[] remark;

    //对应数据库blob类型
    private String description;

    private Map map;

    //这个属性在数据库中不存在
    private String tmpCol;

    // 对应数据库中的json类型 注意这里不要用String接收 否则在返回给前端调用toJson时会把引号 转义
    // 应该根据json格式定义一个类，如果不想定义可以用Object类型(会实例化一个LinkedHashMap赋值给Object)
    private Object other;


    //头衔 对应[json]类型 数据库中保存["A","B","C"]格式
    private List<String> titles;
    private String[] labels;
    private int[] scores;

    //对应varchar 数据库中保存A,B,C格式
    private List<String> ctitles;
    private String[] clabels;
    private int[] cscores;

    //对应数据类型point
    private Double[] workLocation;

    //对应数据类型point
    private Point homeLocation;

    private LocalTime localTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }

    public String getWorkCode() {
        return workCode;
    }

    public void setWorkCode(String workCode) {
        this.workCode = workCode;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getJoinYmd() {
        return joinYmd;
    }

    public void setJoinYmd(String joinYmd) {
        this.joinYmd = joinYmd;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public byte[] getRemark() {
        return remark;
    }

    public void setRemark(byte[] remark) {
        this.remark = remark;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public String getTmpCol() {
        return tmpCol;
    }

    public void setTmpCol(String tmpCol) {
        this.tmpCol = tmpCol;
    }

    public Object getOther() {
        return other;
    }

    public void setOther(Object other) {
        this.other = other;
    }

    public List<Integer> getTypes() {
        return types;
    }

    public void setTypes(List<Integer> types) {
        this.types = types;
    }

    public double[] getDouble_array() {
        return double_array;
    }

    public void setDouble_array(double[] double_array) {
        this.double_array = double_array;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public String[] getLabels() {
        return labels;
    }

    public void setLabels(String[] labels) {
        this.labels = labels;
    }

    public int[] getScores() {
        return scores;
    }

    public void setScores(int[] scores) {
        this.scores = scores;
    }

    public List<String> getCtitles() {
        return ctitles;
    }

    public void setCtitles(List<String> ctitles) {
        this.ctitles = ctitles;
    }

    public String[] getClabels() {
        return clabels;
    }

    public void setClabels(String[] clabels) {
        this.clabels = clabels;
    }

    public int[] getCscores() {
        return cscores;
    }

    public void setCscores(int[] cscores) {
        this.cscores = cscores;
    }

    public Double[] getWorkLocation() {
        return workLocation;
    }

    public void setWorkLocation(Double[] workLocation) {
        this.workLocation = workLocation;
    }

    public Point getHomeLocation() {
        return homeLocation;
    }

    public void setHomeLocation(Point homeLocation) {
        this.homeLocation = homeLocation;
    }

    public LocalTime getLocalTime() {
        return localTime;
    }

    public void setLocalTime(LocalTime localTime) {
        this.localTime = localTime;
    }
}
