package org.anyline.simple.mysql;

import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.TableBuilder;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.data.prepare.RunPrepare;
import org.anyline.metadata.Table;
import org.anyline.proxy.ServiceProxy;
import org.anyline.util.ConfigTable;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = MySQLApplication.class)
public class QueryTest {
    private Logger log = LoggerFactory.getLogger(QueryTest.class);
    /* **********************************************************************************
    *
    *                           init或join方法中提供RunPrepare的把prepare作为子查询
    *
    * ***********************************************************************************/
    /**
     * 先创建测试表
     * @throws Exception Exception
     */
    @Test
    public void init() throws Exception {
        init("FI_USER");
        init("HR_USER");
        init("MM_USER");
        init("HR_TYPE");
    }
    public void init(String name) throws Exception {
        Table table = ServiceProxy.metadata().table(name, false);
        if(null != table){
            ServiceProxy.ddl().drop(table);
        }
        table = new Table(name);
        table.addColumn("ID", "BIGINT").setPrimary(true).setAutoIncrement(true);
        table.addColumn("CODE", "VARCHAR(32)");
        table.addColumn("NAME", "VARCHAR(10)");
        table.addColumn("TYPE_CODE", "VARCHAR(10)");
        table.addColumn("LVL", "INT");
        table.addColumn("REMARK", "VARCHAR(100)");
        ServiceProxy.ddl().create(table);
    }
    @Test
    public void table1(){
        ServiceProxy.querys("FI_USER");
        //SELECT * FROM FI_USER
    }
    @Test
    public void table2(){
        ServiceProxy.querys("FI_USER(ID, CODE AS USER_CODE)");
        //SELECT ID, CODE AS USER_CODE FROM FI_USER
    }
    @Test
    public void sql(){
        ServiceProxy.querys("SELECT * FROM FI_USER");
    }
    @Test
    public void builder1(){
        RunPrepare prepare = TableBuilder.init("FI_USER").build();
        ServiceProxy.querys(prepare, "((ID>0 and code<10) or code != 1)", "code:1");
        //SELECT * FROM FI_USER
    }
    @Test
    public void builder2(){
        //表名(列,列)
        RunPrepare prepare = TableBuilder.init("FI_USER(ID AS USER_ID, CODE) AS FI").build();
        ServiceProxy.querys(prepare);
        //SELECT  ID AS USER_ID, CODE FROM FI_USER
    }
    @Test
    public void builder3(){
        //表名(列,列) AS 表别名
        RunPrepare prepare = TableBuilder.init("FI_USER(ID AS USER_ID, CODE) AS M").build();
        ServiceProxy.querys(prepare);
        //SELECT  ID AS USER_ID, CODE FROM FI_USER AS M
    }
    @Test
    public void fk(){
        RunPrepare prepare = TableBuilder.init("HR_USER").foreign("TYPE_CODE", "HR_TYPE", "CODE", "NAME", "TYPE_NAME").build();
        ServiceProxy.querys(prepare);
        /*
            SELECT
                HR_TYPE.NAME AS TYPE_NAME, HR_USER.*
            FROM HR_USER
            LEFT JOIN HR_TYPE ON HR_USER.TYPE_CODE = HR_TYPE.CODE
        */
        prepare = TableBuilder.init("HR_USER AS U").foreign("TYPE_CODE", "HR_TYPE AS T", "CODE", "NAME", "TYPE_NAME", "U.ID > 0").build();
        ServiceProxy.querys(prepare);
        /*
            SELECT
                T.NAME AS TYPE_NAME, U.*
            FROM HR_USER AS U
            LEFT JOIN HR_TYPE AS T ON (U.TYPE_CODE = T.CODE AND U.ID > 0)
        */
    }
    @Test
    public void builder_join1(){
        /*
        *       SELECT  * FROM FI_USER AS FI
        *       LEFT JOIN HR_USER AS HR ON FI.ID = HR.ID
        */

        /* *************************************************************************************************************
        *
        *                                                 1) 后台TableBuilder直接构造
        *
        * *************************************************************************************************************/
        ConfigStore configs = new DefaultConfigStore();
        configs.and("FI.NAME = HR.ID");
        TableBuilder builder = TableBuilder.init("FI_USER AS FI")                           //主表
            .left("HR_USER AS HR", configs,"FI.ID = HR.ID");                            //关联表 + 关联条件
        RunPrepare prepare = builder.build();
        ServiceProxy.querys(prepare);


        /* *************************************************************************************************************
         *
         *                                                 2) TableBuilder > JSON
         *
         * *************************************************************************************************************/
        String json = builder.json();
        log.info("[TableBuilder > json]\n{}",json);


        /* *************************************************************************************************************
         *
         *                                                 3) JSON > TableBuilder
         *
         * *************************************************************************************************************/
        prepare = TableBuilder.build(json);                                                      //解析JSON
        ServiceProxy.querys(prepare);
        json = builder.json();                                                                   //还原JSON检查一下格式
        log.info("[还原json]\n{}",json);


        /* *************************************************************************************************************
         *
         *                                                 4) 简化JSON
         *
         * *************************************************************************************************************/
        /*
         {
            "table": "FI_USER",
            "alias": "FI",
            "joins": [
                {
                    "table": "HR_USER",
                    "alias": "HR",
                    "type": "LEFT",
                    "conditions": "FI.ID = HR.ID"
                }
            ]
        }
        * */
        json = "{\"table\":\"FI_USER\",\"alias\":\"FI\",\"joins\":[{\"table\":\"HR_USER\",\"alias\":\"HR\",\"type\":\"LEFT\",\"conditions\":\"FI.ID = HR.ID\"}]}";
        prepare = TableBuilder.build(json);
        ServiceProxy.querys(prepare);
    }

    /**
     * 指定查询列
     * 可以在表名名指定
     */
    @Test
    public void builder_join2(){
        /*
        *   SELECT DISTINCT
        *       FI.ID AS FI_ID, HR.ID AS HR_ID
        *   FROM FI_USER AS FI
        *   LEFT JOIN HR_USER AS HR ON FI.ID = HR.ID
        */

        /* *************************************************************************************************************
         *
         *                                                 1) 后台TableBuilder直接构造
         *
         * *************************************************************************************************************/
        TableBuilder builder = TableBuilder.init("FI_USER(DISTINCT FI.ID AS FI_ID, HR.ID AS HR_ID) AS FI")
            .left("HR_USER AS HR", "FI.ID = HR.ID");
        RunPrepare prepare = builder.build();
        ServiceProxy.querys(prepare);


        /* *************************************************************************************************************
         *
         *                                                 2) TableBuilder > JSON
         *
         * *************************************************************************************************************/
        String json = builder.json();
        log.info("[TableBuilder > json]\n{}",json);


        /* *************************************************************************************************************
         *
         *                                                 3) JSON > TableBuilder
         *
         * *************************************************************************************************************/
        prepare = TableBuilder.build(json);                                                      //解析JSON
        ServiceProxy.querys(prepare);
        json = builder.json();                                                                   //还原JSON检查一下格式
        log.info("[还原json]\n{}",json);

    }
    /**
     * 指定查询列
     * 可以在表名名指定
     */
    @Test
    public void builder_join2_condition(){
        /*
         *   SELECT
         *        FI.ID AS FI_ID, HR.ID AS HR_ID
         *   FROM FI_USER AS FI
         *   LEFT JOIN HR_USER AS HR ON FI.ID = HR.ID
         *   WHERE FI.ID = ?
         */

        /* *************************************************************************************************************
         *
         *                                                 1) 后台TableBuilder直接构造
         *
         * *************************************************************************************************************/
        TableBuilder builder = TableBuilder.init("FI_USER(FI.ID AS FI_ID, HR.ID AS HR_ID) AS FI")
            .left("HR_USER AS HR", "FI.ID = HR.ID");
        RunPrepare prepare = builder.build();
        ServiceProxy.querys(prepare, "FI.ID:1::bigint");


        /* *************************************************************************************************************
         *
         *                                                 2) TableBuilder > JSON
         *
         * *************************************************************************************************************/
        String json = builder.json();
        log.info("[TableBuilder > json]\n{}",json);


        /* *************************************************************************************************************
         *
         *                                                 3) JSON > TableBuilder
         *
         * *************************************************************************************************************/
        prepare = TableBuilder.build(json);                                                      //解析JSON
        ServiceProxy.querys(prepare, "FI.ID:1::bigint");
        json = builder.json();                                                                   //还原JSON检查一下格式
        log.info("[还原json]\n{}",json);
    }
    /**
     * 指定查询列
     * 也可以单独指定
     */
    @Test
    public void builder_join3(){
        /*
        *   SELECT distinct
        *       FI.ID AS FI_ID, HR.ID AS HR_ID
        *   FROM FI_USER AS FI
        *   LEFT JOIN HR_USER AS HR ON FI.ID = HR.ID
        */
        /* *************************************************************************************************************
         *
         *                                                 1) 后台TableBuilder直接构造
         *
         * *************************************************************************************************************/
        TableBuilder builder = TableBuilder.init("FI_USER AS FI")
            .left("HR_USER AS HR", "FI.ID = HR.ID")
            .columns("FI.ID AS FI_ID", "HR.ID AS HR_ID").distinct(true);
        RunPrepare prepare = builder.build();
        ServiceProxy.querys(prepare);


        /* *************************************************************************************************************
         *
         *                                                 2) TableBuilder > JSON
         *
         * *************************************************************************************************************/
        String json = builder.json();
        log.info("[TableBuilder > json]\n{}",json);


        /* *************************************************************************************************************
         *
         *                                                 3) JSON > TableBuilder
         *
         * *************************************************************************************************************/
        prepare = TableBuilder.build(json);                                                      //解析JSON
        ServiceProxy.querys(prepare);
        json = builder.json();                                                                   //还原JSON检查一下格式
        log.info("[还原json]\n{}",json);
    }
    @Test
    public void builder_inner1(){
        //子查询
        RunPrepare inner_hr = TableBuilder.init("HR_USER(ID AS HR_ID, CODE AS HR_CODE) AS HR").build();

        RunPrepare master = TableBuilder.init("FI_USER(M.ID AS FI_ID, HRS.HR_CODE) AS M")   //()内指定的是最外层的查询列名，放在主表名容易误解，可以addColumns()单独指定
            .inner("HRS", inner_hr, "HRS.HR_ID = M.ID", "HRS.HR_CODE = M.CODE")    //主表的表名列名要用原名 这里的子查的表名列名注意用 别名 HRS是当前子查询的别名
            //.columns("M.ID AS ID1", "M.ID AS ID2", "HR.HR_ID AS ID3")                            //设置查询列名，注意是追加不会覆盖  覆盖用setColumns()
            .build();
        ServiceProxy.querys(master);
        /* 注意区分内外层 别名
        SELECT
            M.ID AS FI_ID, HRS.HR_CODE
        FROM FI_USER AS M
        LEFT JOIN (
            SELECT
                ID AS HR_ID, CODE AS HR_CODE
            FROM HR_USER AS HR
        ) AS HRS ON (HRS.HR_ID = M.ID AND HRS.HR_CODE = M.CODE)
        */
    }

    @Test
    public void builder_inner_condition(){
        //子查询
        ConfigStore configs = new DefaultConfigStore();
        configs.and("ID", ""); //空条件忽略
        configs.and("CODE='1'");
        configs.and("LVL", 2);
        RunPrepare inner_fi = TableBuilder.init("FI_USER(ID AS FI_ID, CODE AS FI_CODE, 'FI' AS BIZ_TYPE_CODE) AS FI").condition(configs).build();
        configs = new DefaultConfigStore();
        configs.and("ID", "");//空条件忽略
        configs.and("CODE","10");
        configs.and("LVL", 20);
        RunPrepare inner_hr = TableBuilder.init("HR_USER(ID AS HR_ID, CODE AS HR_CODE) AS HR").condition(configs).build();

        RunPrepare group_mm = TableBuilder.init("HR_USER(TYPE_CODE, LVL, MAX(ID) AS MAX_ID) AS MM").build().group("TYPE_CODE", "LVL").having("MAX(ID) > 10");


        RunPrepare master = TableBuilder.init("FIS", inner_fi)   //主表也用一个子查询
            .left("HRS", inner_hr, "HRS.HR_ID = FIS.FI_ID", "HRS.HR_CODE = FIS.FI_CODE")                  //主表的表名列名要用原名 这里的子查的表名列名注意用 别名
            .left("MMS", group_mm, "MMS.MAX_ID = FIS.FI_ID")
            .setColumns("FIS.FI_ID AS FI_IDS","1 AS STATIC_VALUE", "FIS.BIZ_TYPE_CODE") //注意里这里要用外层别名
            .build();
        ServiceProxy.querys(master, "HRS.HR_ID > 3", "HRS.HR_CODE:30::int");
        /*
            SELECT
                FIS.FI_ID AS FI_IDS, 1 AS STATIC_VALUE, FIS.BIZ_TYPE_CODE
            FROM (
                SELECT
                    ID AS FI_ID, CODE AS FI_CODE, 'FI' AS BIZ_TYPE_CODE
                FROM FI_USER AS FI
                WHERE (CODE=1 AND FI.LVL = ?)
            ) AS FIS
            LEFT JOIN (
                SELECT
                    ID AS HR_ID, CODE AS HR_CODE
                FROM HR_USER AS HR
                WHERE (CODE=10 AND HR.LVL = ?)
            ) AS HRS ON (HRS.HR_ID = FIS.FI_ID AND HRS.HR_CODE = FIS.FI_CODE)
            LEFT JOIN (
                SELECT
                    TYPE_CODE, LVL, MAX(ID) AS MAX_ID
                FROM HR_USER AS MM
                    GROUP BY TYPE_CODE, LVL HAVING MAX(ID) > 10
            ) AS MMS ON MMS.MAX_ID = FIS.FI_ID
            WHERE (HRS.HR_ID > 3 AND HRS.HR_CODE = ?)

            param0=2(java.lang.String)
            param1=20(java.lang.String)
            param2=30(java.lang.String)
        */
    }

    @Test
    public void exists(){
        ConfigTable.IS_AUTO_CHECK_METADATA = true;
        TableBuilder builder = TableBuilder.init("HR_USER AS H");
        RunPrepare prepare = builder.build();
        ConfigStore cfs = prepare.condition();
        cfs.and("H.ID = F.ID");
        cfs.and("H.CODE", "");
        cfs.and("H.ID", 100);
        ConfigStore configs = new DefaultConfigStore();
        configs.exists(prepare);
        Table table = new Table("FI_USER");
        table.setAlias("F");
        ServiceProxy.querys(table, configs);
    }

    @Test
    public void distinct(){
        ConfigStore configs = new DefaultConfigStore();
        configs.distinct(true);
        configs.columns("ID", "CODE");
        ServiceProxy.query("CRM_USER", configs);
        configs.columns(true, "ID", "CODE");
        ServiceProxy.query("CRM_USER", configs);
    }

}
