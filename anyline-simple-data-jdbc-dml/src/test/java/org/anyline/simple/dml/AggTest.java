package org.anyline.simple.dml;

import org.anyline.data.param.AggregationBuilder;
import org.anyline.data.param.Config;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.TableBuilder;
import org.anyline.data.param.init.DefaultConfig;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.data.prepare.RunPrepare;
import org.anyline.entity.*;
import org.anyline.entity.generator.GeneratorConfig;
import org.anyline.entity.generator.PrimaryGenerator;
import org.anyline.metadata.Table;
import org.anyline.proxy.ServiceProxy;
import org.anyline.proxy.TransactionProxy;
import org.anyline.service.AnylineService;
import org.anyline.simple.dml.entity.User;
import org.anyline.util.BeanUtil;
import org.anyline.util.ConfigTable;
import org.anyline.util.regular.Regular;
import org.anyline.util.regular.RegularUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
public class AggTest {
    private Logger log = LoggerFactory.getLogger(AggTest.class);

    @Autowired
    private AnylineService service          ;

    @Test
    public void init() throws Exception{
        Table table = service.metadata().table("TAB_AGG", false);
        if(null != table){
            service.ddl().drop(table);
        }
        table = new Table("TAB_AGG");
        table.addColumn("ID", "BIGINT").setAutoIncrement(true).setPrimary(true);
        table.addColumn("SALARY", "DECIMAL(10,2)");
        table.addColumn("TYPE_CODE", "INT");
        table.addColumn("DEPT_CODE", "INT");
        table.addColumn("NAME", "VARCHAR(10)");
        service.ddl().create(table);
    }
    @Test
    public void simple(){
        service.querys("TAB_AGG(AVG(SALARY) AS SALARY_AVG, MAX(SALARY) AS SALARY_MAX)"
                , "GROUP BY TYPE_CODE"
                , "HAVING COUNT(*) > 1"
        );
    }
    @Test
    public void table(){
        Table table = new Table("TAB_AGG(DEPT_CODE, TYPE_CODE)");
        table.aggregation(Aggregation.COUNT, "*", "QTY");
        table.aggregation(Aggregation.COUNT, "DISTINCT SALARY", "DISTINCT_QTY");
        table.aggregation(Aggregation.MAX, "SALARY", "SALARY_MAX");
        table.aggregation(Aggregation.AVG, "SALARY", "SALARY_AVG");
        table.group("TYPE_CODE", "DEPT_CODE");
        table.having("count(*) > 1");
        DataSet set = service.querys(table, "ID>10");
        System.out.println(set);
    }
    @Test
    public void service(){
        service.aggregation()
                .table("TAB_AGG")
                .aggregation(Aggregation.COUNT, "*", "QTY")
                .aggregation(Aggregation.COUNT, "DISTINCT SALARY", "DISTINCT_QTY")
                .aggregation(Aggregation.MAX, "SALARY", "SALARY_MAX")
                .aggregation(Aggregation.AVG, "SALARY", "SALARY_AVG")
                .group("TYPE_CODE", "DEPT_CODE")
                .order("TYPE_CODE")
                .having("count(*) > 1")
                .querys();
    }
    @Test
    public void prepare(){
        RunPrepare prepare = TableBuilder.init("TAB_AGG").build();
        prepare.aggregation(Aggregation.COUNT, "*", "QTY");
        prepare.aggregation(Aggregation.COUNT, "DISTINCT SALARY", "DISTINCT_QTY");
        prepare.aggregation(Aggregation.MAX, "SALARY", "SALARY_MAX");
        prepare.aggregation(Aggregation.AVG, "SALARY", "SALARY_AVG");
        prepare.group("TYPE_CODE", "DEPT_CODE");
        prepare.having("count(*) > 1");
        DataSet set = service.querys(prepare, "ID>10");
    }
}
