package org.anyline.simple.voltdb;


import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.service.AnylineService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
public class VoltDBApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(VoltDBApplication.class);
        application.run(args);
    }
}
