package org.anyline.simple.controller;

import com.zaxxer.hikari.HikariDataSource;
import org.anyline.controller.impl.AnylineController;
import org.anyline.data.datasource.DataSourceHolder;
import org.anyline.entity.DataRow;
import org.anyline.proxy.ServiceProxy;
import org.anyline.simple.service.SSOService;
import org.anyline.util.BasicUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("sso")
public class SSOController extends AnylineController {

    @Autowired
    private SSOService sso;

    @RequestMapping("a")
    public String add(HttpServletRequest request){
        try {
            if(!DataSourceHolder.contains("sso")) {
                HikariDataSource ds = new HikariDataSource();
                ds.setJdbcUrl("jdbc:mysql://localhost:33306/simple_sso?useUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true");
                ds.setUsername("root");
                ds.setPassword("root");
                ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
                DataSourceHolder.reg("sso", ds);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        service = ServiceProxy.service("sso");
        long qty = service.count("SSO_USER");
        try {
            DataRow row = service.query("sso_user");
            row.put("NM", BasicUtil.getRandomString(10));
            sso.test("sso");
        }catch (Exception e){
            e.printStackTrace();
        }
        long cnt = service.count("SSO_USER");

        return success("insert前后行数:"+qty+">"+cnt);
    }
}
