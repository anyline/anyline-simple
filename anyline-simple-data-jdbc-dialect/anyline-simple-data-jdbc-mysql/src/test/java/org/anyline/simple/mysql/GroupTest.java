package org.anyline.simple.mysql;

import org.anyline.data.param.ConfigBuilder;
import org.anyline.data.param.ConfigStore;
import org.anyline.data.param.TableBuilder;
import org.anyline.data.param.init.DefaultConfigStore;
import org.anyline.data.prepare.RunPrepare;
import org.anyline.metadata.Table;
import org.anyline.proxy.ServiceProxy;
import org.anyline.util.ConfigTable;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = MySQLApplication.class)
public class GroupTest {
    private Logger log = LoggerFactory.getLogger(GroupTest.class);
    /* **********************************************************************************
    *
    *                           init或join方法中提供RunPrepare的把prepare作为子查询
    *
    * ***********************************************************************************/
    /**
     * 先创建测试表
     * @throws Exception Exception
     */
    @Test
    public void init() throws Exception {
        init("FI_USER");
    }
    public void init(String name) throws Exception {
        Table table = ServiceProxy.metadata().table(name, false);
        if(null != table){
            ServiceProxy.ddl().drop(table);
        }
        table = new Table(name);
        table.addColumn("ID", "BIGINT").setPrimary(true).setAutoIncrement(true);
        table.addColumn("CODE", "VARCHAR(32)");
        table.addColumn("NAME", "VARCHAR(10)");
        table.addColumn("TYPE_CODE", "VARCHAR(10)");
        table.addColumn("LVL", "INT");
        table.addColumn("REMARK", "VARCHAR(100)");
        ServiceProxy.ddl().create(table);
    }
    @Test
    public void group(){
        RunPrepare prepare = TableBuilder.init("FI_USER(TYPE_CODE, LVL, MAX(ID) AS MAX_ID) AS MM").build()
            .group("TYPE_CODE", "LVL")
            .having("MAX(ID) > 10")
            .having("COUNT(*) >1");
        ServiceProxy.querys(prepare);
    }
    @Test
    public void group2(){
        ConfigStore having = new DefaultConfigStore();
        having.and("MIN(ID) > 1");
        having.ge("MIN(ID)", 2);

        RunPrepare prepare = TableBuilder.init("FI_USER(TYPE_CODE, LVL, MAX(ID) AS MAX_ID) AS MM").build()
            .group("TYPE_CODE", "LVL")
            .having("MAX(ID) > 10")
            .having(having);
        ServiceProxy.querys(prepare);
        /*
SELECT
	TYPE_CODE, LVL, MAX(ID) AS MAX_ID
FROM FI_USER AS MM
GROUP BY TYPE_CODE, LVL
HAVING (MAX(ID) > 10 AND MIN(ID) > 1)
        */
    }
    @Test
    public void hav(){
        String json = "{\"columns\":{\"query\":[\"COUNT(T1.id) as countCol_4c02\",\"MAX(T1.id) as countCol_c690\",\"Td8a1.rr896_o525 as `Td8a1.rr896_o525`\",\"case when Td8a1.rr896_o525  \\u003d  \\u0027工程图纸\\u0027 then \\u0027工程图纸测试\\u0027 when Td8a1.rr896_o525  \\u003d  \\u0027工程档案\\u0027 then \\u0027工程档案测试\\u0027 else \\u0027其他\\u0027  end as `newCol_1388` \"]},\"conditions\":{\"items\":[{\"var\":\"STATUS\",\"values\":[0],\"join\":\"AND\",\"compare\":10}],\"join\":\"AND\"},\"navi\":{\"page\":1,\"vol\":10,\"auto_count\":false},\"havings\":[{\"var\":\"countCol_4c02\",\"values\":[20],\"text\":\"\",\"join\":\"and\",\"compare\":10},{\"var\":\"countCol_c690\",\"values\":null,\"text\":\"\",\"join\":\"and\",\"compare\":10}]}";
        ConfigStore configs = ConfigBuilder.build(json);

        configs.group("abc");
        ServiceProxy.querys("crm_user", configs);

    }
}
