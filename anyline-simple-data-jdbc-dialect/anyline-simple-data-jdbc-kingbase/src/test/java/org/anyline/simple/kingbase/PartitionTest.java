package org.anyline.simple.kingbase;

import org.anyline.entity.DataRow;
import org.anyline.metadata.*;
import org.anyline.metadata.Table.Partition;
import org.anyline.service.AnylineService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.time.LocalDate;

@SpringBootTest
public class PartitionTest {
    private Logger log = LoggerFactory.getLogger(PartitionTest.class);
    @Autowired
    private AnylineService service          ;
    @Autowired
    private JdbcTemplate jdbc               ;
    private Catalog catalog  = null          ; // 可以相当于数据库名
    private Schema schema   = null          ; // 如 dbo
    private String table    = "CRM_USERS"    ; // 表名

    /**
     * 分区表
     */
    @Test
    public void partition_range() throws Exception{
        String name = "user_master_range";
        //根据范围分区
        Table table = service.metadata().table(name, false);
        if(null != table){
            service.ddl().drop(table);
        }
        MasterTable master = new MasterTable(name);
        master.addColumn("ID", "INT");
        master.addColumn("NAME", "VARCHAR(10)").setComment("姓名");

        Partition partition = new Partition();
        partition.setType(Partition.TYPE.RANGE);
        partition.addColumn("ID");
        master.setPartition(partition);
        //或者
        master.partitionBy(Partition.TYPE.RANGE, "ID");
        service.ddl().create(master);

        PartitionTable log1 = new PartitionTable(name + "_P1");
        log1.setMaster(master);
        log1.setPartition(new Partition(Partition.TYPE.RANGE).setMin(0).setMax(99));
        service.ddl().create(log1);
        PartitionTable log2 = new PartitionTable(name + "_P2");
        log2.setMaster(master);
        //也可以不设置type共用主表的type
        log2.setPartition(new Partition().setMin(100).setMax(199));
        service.ddl().create(log2);
        DataRow log = new DataRow();
        log.put("ID", 111);
        log.put("NAME","z");
        service.insert(name, log);

        //从主表或相就的分区表中可以查到
        service.querys(name);
        service.querys(name + "_p2");


        table = service.metadata().table(name);
        partition = table.getPartition();
        Partition.TYPE type = partition.getType();
        System.out.println("partition type:" + type);
    } 
    @Test
    public void partition_range_date() throws Exception{
        String name = "user_master_range_date";
        //根据时间范围分区
        Table table = service.metadata().table(name, false);
        if(null != table){
            service.ddl().drop(table);
        }
        MasterTable master = new MasterTable(name);
        master.addColumn("ID", "INT");
        master.addColumn("YMD", "DATE").setComment("日期");
        master.addColumn("NAME", "VARCHAR(10)").setComment("姓名");
        //设置主表分区依据的列
        master.partitionBy(Partition.TYPE.RANGE, "YMD");
        service.ddl().create(master);

        PartitionTable log1 = new PartitionTable(name + "_P1");
        log1.setMaster(master);
        //设置分区范围
        log1.setPartition(new Partition(Partition.TYPE.RANGE).setMin("2020-01-01").setMax("2020-01-31"));
        service.ddl().create(log1);
        PartitionTable log2 = new PartitionTable(name + "_P2");
        log2.setMaster(master);
        //也可以不设置type共用主表的type
        log2.setPartition(new Partition().setMin("2020-02-01").setMax("2020-05-31"));
        service.ddl().create(log2);
        DataRow log = new DataRow();
        log.put("ID", 111);
        log.put("YMD",  LocalDate.of(2020,3,1));
        log.put("NAME","z");
        service.insert(name, log);

        //从主表或相就的分区表中可以查到
        service.querys(name);
        service.querys(name + "_P2");
    }
    @Test
    public void partition_list() throws Exception{
        String name = "user_master_list";
        //根据部门编号 分区
        Table table = service.metadata().table(name, true);
        if(null != table){
            service.ddl().drop(table);
        }
        MasterTable master = new MasterTable(name);
        master.addColumn("ID", "INT");
        master.addColumn("NAME", "VARCHAR(10)").setComment("姓名");
        master.addColumn("DEPT_CODE", "VARCHAR(10)").setComment("部门ID");

        //或者
        master.partitionBy(Partition.TYPE.LIST, "DEPT_CODE");
        service.ddl().create(master);

        PartitionTable sd = new PartitionTable(name + "_SD_PP");
        sd.setMaster(master);
        sd.setPartition(new Partition(Partition.TYPE.LIST).addValues("SD", "PP"));
        service.ddl().create(sd);
        PartitionTable fi = new PartitionTable(name + "_FI_CO");
        fi.setMaster(master);
        fi.setPartition(new Partition(Partition.TYPE.LIST).addValues("FI", "CO"));
        service.ddl().create(fi);

        DataRow user = new DataRow();
        user.put("DEPT_CODE", "FI");//部门编号必须是子表 分区依据中出现的 并 操持大小写一致
        user.put("NAME","ZH_FI");
        //通过主表插入
        service.insert(name, user);


        //从主表或相就的分区表中可以查到
        service.querys(name);
        service.querys(name + "_FI_CO");
    }
    @Test
    public void partition_hash() throws Exception{
        String name = "user_master_hash";
        Table table = service.metadata().table(name, false);
        if(null != table){
            service.ddl().drop(table);
        }
        MasterTable master = new MasterTable(name);
        master.addColumn("ID", "INT");
        master.addColumn("NAME", "VARCHAR(10)").setComment("姓名");
        master.addColumn("DEPT_CODE", "int").setComment("部门ID");
        Partition partition = new Partition();
        partition.setType(Partition.TYPE.HASH).setColumns("ID");
        master.setPartition(partition);
        service.ddl().create(master);

        PartitionTable u1 = new PartitionTable(name + "_U1");
        u1.setMaster(master);
        u1.setPartition(new Partition().setHash(3,0));
        service.ddl().create(u1);
        PartitionTable u2 = new PartitionTable(name + "_U2");
        u2.setMaster(master);
        u2.setPartition(new Partition().setHash(3,1));
        service.ddl().create(u2);
        PartitionTable u3 = new PartitionTable(name + "_U3");
        u3.setMaster(master);
        u3.setPartition(new Partition().setHash(3,2));
        service.ddl().create(u3);

        DataRow user = new DataRow();
        user.put("DEPT_CODE",5);//部门编号必须是子表 分区依据中出现的 并 操持大小写一致
        user.put("NAME","ZH_FI");
        //通过主表插入
        service.insert(name, user);


        //从主表或相就的分区表中可以查到
        service.querys(name);
        service.querys(name + "_u3");
    }

}
