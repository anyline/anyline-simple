package org.anyline.simple.ds;

import org.anyline.entity.VariableValue;
import org.anyline.metadata.*;
import org.anyline.service.AnylineService;
import org.anyline.util.BasicUtil;
import org.anyline.util.regular.RegularUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@SpringBootTest
public class ParseTest {
    private Logger log = LoggerFactory.getLogger(ParseTest.class);
    @Autowired
    private AnylineService service          ;
    @Autowired
    private JdbcTemplate jdbc               ;
    private Catalog catalog = null          ; //
    private Schema schema   = null          ; //相当于数据库名  查数据库列表 是用SHOW SCHEMAS 但JDBC con.getCatalog()返回数据库名 而con.getSchema()返回null
    private String table    = "crm_user"    ; // 表名

    public static void main(String[] args) throws Exception{
        String pg = "CREATE TABLE public.bs_component_group (\n" +
                "  id int4 NOT NULL DEFAULT nextval('bs_component_group_id_seq'::regclass),\n" +
                "  fid varchar(50) COLLATE pg_catalog.default,\n" + 
                "  enterprise_id int8,\n" + 
                "  data_status int4 DEFAULT 1,\n" +
                "  data_version varchar(50) COLLATE pg_catalog.default DEFAULT '0'::character varying,\n" +
                "  app_code varchar(50) COLLATE pg_catalog.default,\n" + 
                "  data_record_id int8,\n" +
                "  facility_id int8,\n" + 
                "  floor_code varchar(50) COLLATE pg_catalog.default,\n" + 
                "  CONSTRAINT pk_bs_component_group PRIMARY KEY (id)\n" +
                ")\n" +
                ";\n" +
                "\n" +
                "ALTER TABLE public.bs_component_group \n" +
                "  OWNER TO postgres;\n" +
                "\n" +
                "COMMENT ON COLUMN public.bs_component_group.id IS 'ID';\n" +
                "\n" +
                "COMMENT ON COLUMN public.bs_component_group.fid IS '第三方ID';\n" +
                "\n" +
                "COMMENT ON COLUMN public.bs_component_group.code IS 'CODE';\n" +
                "\n" +
                "COMMENT ON TABLE public.bs_component_group IS 'BS_密封点群组';";
        parse(pg);
        String mysql = "CREATE TABLE `tab_parse` (\n" +
                "  `id` bigint NOT NULL AUTO_INCREMENT,\n" +
                "  `TYPE` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型',\n" +
                "  `CODE` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL COMMENT '号码',\n" +
                "  `Device` bigint DEFAULT NULL COMMENT '定位设备编号(no)',\n" +
                "  `DeviceNum_0532` bigint DEFAULT NULL,\n" +
                "  `IS_INIT_TD` int DEFAULT '0',\n" +
                "  PRIMARY KEY (`id`),\n" +
                "  UNIQUE KEY `code` (`code`),\n" +
                "  UNIQUE KEY `qr_code_index` (`qr_code`),\n" +
                "  KEY `DeviceNum` (`DeviceNum`)\n" +
                ") ENGINE=InnoDB AUTO_INCREMENT=111120179 DEFAULT CHARSET=utf8mb3 COMMENT='备注';";
        parse(mysql);

    }
    public static Metadata parse(String ddl) throws Exception{
        //不指定数据库类型的不解析catalog,schema除非catalog与schema都有
        Metadata meta = null;
        String type = RegularUtil.cut(ddl, "CREATE", " ", " ");
        System.out.println(ddl);
        if(null != type){
            type = type.trim().toUpperCase();
            if("TABLE".equals(type)){
                meta = parseTable(ddl);
            }
        }
        return meta;
    }

    /**
     * 拆分出最外层()内部分
     * @param txt 全文
     * @return String
     * @throws Exception 标签结构错误时抛出异常
     */
    public static String body( String txt, String head, String foot) throws Exception {
        String body = "";
        int begin = txt.indexOf(head);
        int end = txt.indexOf(foot);
        int fr = begin;
        int to = end;
        if(fr > to || fr == -1){
            return null;
        }
        while (true) {
            String tmp = txt.substring(fr + head.length(), to);
            if (!tmp.contains(head)) {
                break;
            }
            fr = txt.indexOf(head, fr + 1);
            to = txt.indexOf(foot, to + 1);
            end = to;
        }
        body = txt.substring(begin + head.length(), end);

        return body;
    }

    public static Table parseTable(String ddl) throws Exception{
        Table table = new Table();
        String tmp = ddl.split("\\(")[0].trim();
        String[] tmps = tmp.split(" ");
        String name = tmps[tmps.length-1];
        if(name.contains(".")) {
            tmps = name.split("\\.");
            if(tmps.length == 3){
                table.setCatalog(tmps[0]);
                table.setSchema(tmps[1]);
                table.setName(tmps[2]);
            }else{
                table.setName(tmps[tmps.length-1]);
            }
        }else{
            table.setName(name);
        }
        String body = body(ddl, "(", ")");
        parseTableBody(table, body);

        String property = ddl.substring(ddl.indexOf(body)+body.length()+1);
        String engine = RegularUtil.cut(true, property, "ENGINE", "=", " ");
        String charset = RegularUtil.cut(true, property, "CHARSET", "=", " ");
        String comment = RegularUtil.cut(true, property, "COMMENT", "'", "'");
        String collate = afterChars(true, false, property, "COLLATE=");
        if(null != collate){
            collate = collate.replace(";", "");
            table.setCollate(collate);
        }
        table.setCharset(charset);
        table.setEngine(engine);
        table.setComment(comment);
        return table;
    }
    public static void parseTableBody(Table table, String body){
        List<String> cols = split(body);
        LinkedHashMap<String, Column> columns = new LinkedHashMap<>();
        for(String col:cols){
            col = col.replaceAll("\\s+", " ");
            col = col.replace(" ,", ",");
            String up = col.toUpperCase();
            //mysql
            if(up.startsWith("PRIMARY")){
                PrimaryKey pk = new PrimaryKey();
                String[] kcs = RegularUtil.cut(col, "(", ")").split(",");
                for(String kc:kcs){
                    pk.addColumn(kc);
                }
                table.setPrimaryKey(pk);
                continue;
            }
            //pg
            //CONSTRAINT pk_bs_component_group PRIMARY KEY (id)
            if(up.startsWith("CONSTRAINT")){
                String name = RegularUtil.cut(true, col, "CONSTRAINT", " ");
                if(up.contains("PRIMARY KEY")){
                    PrimaryKey pk = new PrimaryKey();
                    pk.setName(name);
                    pk.setTable(table);
                    String[] kcs = RegularUtil.cut(col, "(", ")").split(",");
                    for(String kc:kcs){
                        pk.addColumn(kc);
                    }
                    table.setPrimaryKey(pk);
                }
                continue;
            }
            //mysql
            /*+
                "  UNIQUE KEY `qr_code_index` (`qr_code`),\n" +
                "  KEY `DeviceNum` (`DeviceNum`)\n" +*/
            if(up.startsWith("UNIQUE") || up.startsWith("KEY")){
                Index index = new Index();
                index.setTable(table);
                if(up.startsWith("UNIQUE")){
                    index.setUnique(true);
                }
                String name = RegularUtil.cut(true, col, "KEY", " ", " ");
                index.setName(name);
                String[] kcs = RegularUtil.cut(col, "(", ")").split(",");
                for(String kc:kcs){
                    index.addColumn(kc);
                }
                table.add(index);
                continue;
            }
            String[] tmps = col.split("\\s");
            String name = tmps[0];
            String type = RegularUtil.cut(col, name, " ", " ");
            String comment = RegularUtil.cut(true, col, "commnet", "'", "'");
            boolean auto = up.contains("INCREMENT");
            boolean notNull = up.contains("NOT NULL");
            Object def = null;
            if(up.contains(" DEFAULT ")){
                String afters = afterChars(true, false, col, "DEFAULT");
                if(null != afters){
                    //pg
                    if(afters.toLowerCase().contains("nextval")){
                        auto = true;
                    }else{
                        if(afters.contains("::")){
                            afters = afters.split("::")[0];
                        }else if(afters.contains("(")){
                            def = new VariableValue(afters);
                        }else{
                            def = afters;
                        }
                    }
                }
            }
            String charset = RegularUtil.cut(true, col, "CHARACTER SET", " ", " ");
            String collate = RegularUtil.cut(true, col, "collate", " ", " ");
            Column column = new Column(name, type);
            column.setNullable(!notNull);
            column.setComment(comment);
            column.setAutoIncrement(auto);
            column.setDefaultValue(def);
            column.setCharset(charset);
            column.setCollate(collate);
            columns.put(column.getName().toUpperCase(), column);
        }
        table.setColumns(columns);
    }

    /**
     * keys后的第一个字符
     * @param ignore 是否忽略大小写
     * @param text 原文
     * @param empty 取值时是否包含空
     * @param keys keys
     * @return char
     */
    public static String afterChar(boolean ignore, boolean empty, String text, String ... keys){
        String after = null;
        String up = text.toUpperCase();
        int idx = -1;
        for(String key:keys){
            String k = key;
            if(ignore){
                k = k.toUpperCase();
            }
            idx = up.indexOf(k, idx);
            if(idx == -1){
                return null;
            }
            text = text.substring(idx+key.length());
        }
        if(idx != -1){
            if(!empty){
                text = text.trim();
            }
            after = text.substring(0, 1);
        }
        return after;
    }
    public static String afterChars(boolean ignore, boolean empty, String text, String ... keys){
        String after = null;
        String up = text.toUpperCase();
        int idx = -1;
        for(String key:keys){
            String k = key;
            if(ignore){
                k = k.toUpperCase();
            }
            idx = up.indexOf(k, idx);
            if(idx == -1){
                return null;
            }
            text = text.substring(idx+key.length());
        }
        if(idx != -1){
            if(!empty){
                text = text.trim();
            }
            String c = text.substring(0,1);
            if(c.equals("'")){
                after = RegularUtil.cut(text, "'", "'");
            }else if(c.equals("")){
                after = RegularUtil.cut(text, "", "");
            }else{
                //to_date('2020-12-12 10:10:10', '') comment 'DATE TIME'
                after = RegularUtil.cut(text, RegularUtil.TAG_BEGIN,  " ");
                if(null == after){
                    after = RegularUtil.cut(text, RegularUtil.TAG_BEGIN, ",");
                }
                if(null != after && after.contains("(")){
                    if(BasicUtil.charCount(after, "(") != BasicUtil.charCount(after, ")")){
                        after = RegularUtil.cut(text, RegularUtil.TAG_BEGIN, ")")+")";
                    }
                }
                if(null == after){
                    after = RegularUtil.cut(text, RegularUtil.TAG_BEGIN, RegularUtil.TAG_END);
                }
            }
        }
        return after;
    }
    //根据,分隔 注意识别()内的,
    public static List<String> split(String text){
        List<String> list = new ArrayList<>();
        while (true){
            if(text.isEmpty()){
                break;
            }
            int idx = text.indexOf(",");
            if(idx == -1){
                list.add(text);
                break;
            }
            String before = text.substring(0, idx);
            if(BasicUtil.charCount(before, "(") != BasicUtil.charCount(before, ")")){
                idx = text.indexOf(",", idx+1);
                if(idx != -1) {
                    before = text.substring(0, idx);
                    list.add(before.trim());
                    text = text.substring(idx + 1);
                }
            }else{
                list.add(before.trim());
                text = text.substring(idx+1);
            }
        }
        return list;
    }
}
